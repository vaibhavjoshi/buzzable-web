import{createMarker,NodePart,reparentNodes,removeNodes,directive,css,html,PageViewElement,store,SharedStyles,$myMetterDefault as myPost,getPostdata,deletePost,editPost,connect,loadPage}from"./my-app.js";// TODO(kschaaf): Refactor into Part API?
const createAndInsertPart=(containerPart,beforePart)=>{const container=containerPart.startNode.parentNode,beforeNode=beforePart===void 0?containerPart.endNode:beforePart.startNode,startNode=container.insertBefore(createMarker(),beforeNode);container.insertBefore(createMarker(),beforeNode);const newPart=new NodePart(containerPart.options);newPart.insertAfterNode(startNode);return newPart},updatePart=(part,value)=>{part.setValue(value);part.commit();return part},insertPartBefore=(containerPart,part,ref)=>{const container=containerPart.startNode.parentNode,beforeNode=ref?ref.startNode:containerPart.endNode,endNode=part.endNode.nextSibling;if(endNode!==beforeNode){reparentNodes(container,part.startNode,endNode,beforeNode)}},removePart=part=>{removeNodes(part.startNode.parentNode,part.startNode,part.endNode.nextSibling)},generateMap=(list,start,end)=>{const map=new Map;for(let i=start;i<=end;i++){map.set(list[i],i)}return map},partListCache=new WeakMap,keyListCache=new WeakMap,repeat=directive((items,keyFnOrTemplate,template)=>{let keyFn;if(template===void 0){template=keyFnOrTemplate}else if(keyFnOrTemplate!==void 0){keyFn=keyFnOrTemplate}return containerPart=>{if(!(containerPart instanceof NodePart)){throw new Error("repeat can only be used in text bindings")}// Old part & key lists are retrieved from the last update
// (associated with the part for this instance of the directive)
const oldParts=partListCache.get(containerPart)||[],oldKeys=keyListCache.get(containerPart)||[],newParts=[],newValues=[],newKeys=[];let index=0;for(const item of items){newKeys[index]=keyFn?keyFn(item,index):index;newValues[index]=template(item,index);index++}// Maps from key to index for current and previous update; these
// are generated lazily only when needed as a performance
// optimization, since they are only required for multiple
// non-contiguous changes in the list, which are less common.
let newKeyToIndexMap,oldKeyToIndexMap,oldHead=0,oldTail=oldParts.length-1,newHead=0,newTail=newValues.length-1;// Overview of O(n) reconciliation algorithm (general approach
// based on ideas found in ivi, vue, snabbdom, etc.):
//
// * We start with the list of old parts and new values (and
// arrays of
//   their respective keys), head/tail pointers into each, and
//   we build up the new list of parts by updating (and when
//   needed, moving) old parts or creating new ones. The initial
//   scenario might look like this (for brevity of the diagrams,
//   the numbers in the array reflect keys associated with the
//   old parts or new values, although keys and parts/values are
//   actually stored in parallel arrays indexed using the same
//   head/tail pointers):
//
//      oldHead v                 v oldTail
//   oldKeys:  [0, 1, 2, 3, 4, 5, 6]
//   newParts: [ ,  ,  ,  ,  ,  ,  ]
//   newKeys:  [0, 2, 1, 4, 3, 7, 6] <- reflects the user's new
//   item order
//      newHead ^                 ^ newTail
//
// * Iterate old & new lists from both sides, updating,
// swapping, or
//   removing parts at the head/tail locations until neither
//   head nor tail can move.
//
// * Example below: keys at head pointers match, so update old
// part 0 in-
//   place (no need to move it) and record part 0 in the
//   `newParts` list. The last thing we do is advance the
//   `oldHead` and `newHead` pointers (will be reflected in the
//   next diagram).
//
//      oldHead v                 v oldTail
//   oldKeys:  [0, 1, 2, 3, 4, 5, 6]
//   newParts: [0,  ,  ,  ,  ,  ,  ] <- heads matched: update 0
//   and newKeys:  [0, 2, 1, 4, 3, 7, 6]    advance both oldHead
//   & newHead
//      newHead ^                 ^ newTail
//
// * Example below: head pointers don't match, but tail pointers
// do, so
//   update part 6 in place (no need to move it), and record
//   part 6 in the `newParts` list. Last, advance the `oldTail`
//   and `oldHead` pointers.
//
//         oldHead v              v oldTail
//   oldKeys:  [0, 1, 2, 3, 4, 5, 6]
//   newParts: [0,  ,  ,  ,  ,  , 6] <- tails matched: update 6
//   and newKeys:  [0, 2, 1, 4, 3, 7, 6]    advance both oldTail
//   & newTail
//         newHead ^              ^ newTail
//
// * If neither head nor tail match; next check if one of the
// old head/tail
//   items was removed. We first need to generate the reverse
//   map of new keys to index (`newKeyToIndexMap`), which is
//   done once lazily as a performance optimization, since we
//   only hit this case if multiple non-contiguous changes were
//   made. Note that for contiguous removal anywhere in the
//   list, the head and tails would advance from either end and
//   pass each other before we get to this case and removals
//   would be handled in the final while loop without needing to
//   generate the map.
//
// * Example below: The key at `oldTail` was removed (no longer
// in the
//   `newKeyToIndexMap`), so remove that part from the DOM and
//   advance just the `oldTail` pointer.
//
//         oldHead v           v oldTail
//   oldKeys:  [0, 1, 2, 3, 4, 5, 6]
//   newParts: [0,  ,  ,  ,  ,  , 6] <- 5 not in new map; remove
//   5 and newKeys:  [0, 2, 1, 4, 3, 7, 6]    advance oldTail
//         newHead ^           ^ newTail
//
// * Once head and tail cannot move, any mismatches are due to
// either new or
//   moved items; if a new key is in the previous "old key to
//   old index" map, move the old part to the new location,
//   otherwise create and insert a new part. Note that when
//   moving an old part we null its position in the oldParts
//   array if it lies between the head and tail so we know to
//   skip it when the pointers get there.
//
// * Example below: neither head nor tail match, and neither
// were removed;
//   so find the `newHead` key in the `oldKeyToIndexMap`, and
//   move that old part's DOM into the next head position
//   (before `oldParts[oldHead]`). Last, null the part in the
//   `oldPart` array since it was somewhere in the remaining
//   oldParts still to be scanned (between the head and tail
//   pointers) so that we know to skip that old part on future
//   iterations.
//
//         oldHead v        v oldTail
//   oldKeys:  [0, 1, -, 3, 4, 5, 6]
//   newParts: [0, 2,  ,  ,  ,  , 6] <- stuck; update & move 2
//   into place newKeys:  [0, 2, 1, 4, 3, 7, 6]    and advance
//   newHead
//         newHead ^           ^ newTail
//
// * Note that for moves/insertions like the one above, a part
// inserted at
//   the head pointer is inserted before the current
//   `oldParts[oldHead]`, and a part inserted at the tail
//   pointer is inserted before `newParts[newTail+1]`. The
//   seeming asymmetry lies in the fact that new parts are moved
//   into place outside in, so to the right of the head pointer
//   are old parts, and to the right of the tail pointer are new
//   parts.
//
// * We always restart back from the top of the algorithm,
// allowing matching
//   and simple updates in place to continue...
//
// * Example below: the head pointers once again match, so
// simply update
//   part 1 and record it in the `newParts` array.  Last,
//   advance both head pointers.
//
//         oldHead v        v oldTail
//   oldKeys:  [0, 1, -, 3, 4, 5, 6]
//   newParts: [0, 2, 1,  ,  ,  , 6] <- heads matched; update 1
//   and newKeys:  [0, 2, 1, 4, 3, 7, 6]    advance both oldHead
//   & newHead
//            newHead ^        ^ newTail
//
// * As mentioned above, items that were moved as a result of
// being stuck
//   (the final else clause in the code below) are marked with
//   null, so we always advance old pointers over these so we're
//   comparing the next actual old value on either end.
//
// * Example below: `oldHead` is null (already placed in
// newParts), so
//   advance `oldHead`.
//
//            oldHead v     v oldTail
//   oldKeys:  [0, 1, -, 3, 4, 5, 6] // old head already used;
//   advance newParts: [0, 2, 1,  ,  ,  , 6] // oldHead newKeys:
//   [0, 2, 1, 4, 3, 7, 6]
//               newHead ^     ^ newTail
//
// * Note it's not critical to mark old parts as null when they
// are moved
//   from head to tail or tail to head, since they will be
//   outside the pointer range and never visited again.
//
// * Example below: Here the old tail key matches the new head
// key, so
//   the part at the `oldTail` position and move its DOM to the
//   new head position (before `oldParts[oldHead]`). Last,
//   advance `oldTail` and `newHead` pointers.
//
//               oldHead v  v oldTail
//   oldKeys:  [0, 1, -, 3, 4, 5, 6]
//   newParts: [0, 2, 1, 4,  ,  , 6] <- old tail matches new
//   head: update newKeys:  [0, 2, 1, 4, 3, 7, 6]   & move 4,
//   advance oldTail & newHead
//               newHead ^     ^ newTail
//
// * Example below: Old and new head keys match, so update the
// old head
//   part in place, and advance the `oldHead` and `newHead`
//   pointers.
//
//               oldHead v oldTail
//   oldKeys:  [0, 1, -, 3, 4, 5, 6]
//   newParts: [0, 2, 1, 4, 3,   ,6] <- heads match: update 3
//   and advance newKeys:  [0, 2, 1, 4, 3, 7, 6]    oldHead &
//   newHead
//                  newHead ^  ^ newTail
//
// * Once the new or old pointers move past each other then all
// we have
//   left is additions (if old list exhausted) or removals (if
//   new list exhausted). Those are handled in the final while
//   loops at the end.
//
// * Example below: `oldHead` exceeded `oldTail`, so we're done
// with the
//   main loop.  Create the remaining part and insert it at the
//   new head position, and the update is complete.
//
//                   (oldHead > oldTail)
//   oldKeys:  [0, 1, -, 3, 4, 5, 6]
//   newParts: [0, 2, 1, 4, 3, 7 ,6] <- create and insert 7
//   newKeys:  [0, 2, 1, 4, 3, 7, 6]
//                     newHead ^ newTail
//
// * Note that the order of the if/else clauses is not important
// to the
//   algorithm, as long as the null checks come first (to ensure
//   we're always working on valid old parts) and that the final
//   else clause comes last (since that's where the expensive
//   moves occur). The order of remaining clauses is is just a
//   simple guess at which cases will be most common.
//
// * TODO(kschaaf) Note, we could calculate the longest
// increasing
//   subsequence (LIS) of old items in new position, and only
//   move those not in the LIS set. However that costs O(nlogn)
//   time and adds a bit more code, and only helps make rare
//   types of mutations require fewer moves. The above handles
//   removes, adds, reversal, swaps, and single moves of
//   contiguous items in linear time, in the minimum number of
//   moves. As the number of multiple moves where LIS might help
//   approaches a random shuffle, the LIS optimization becomes
//   less helpful, so it seems not worth the code at this point.
//   Could reconsider if a compelling case arises.
while(oldHead<=oldTail&&newHead<=newTail){if(null===oldParts[oldHead]){// `null` means old part at head has already been used
// below; skip
oldHead++}else if(null===oldParts[oldTail]){// `null` means old part at tail has already been used
// below; skip
oldTail--}else if(oldKeys[oldHead]===newKeys[newHead]){// Old head matches new head; update in place
newParts[newHead]=updatePart(oldParts[oldHead],newValues[newHead]);oldHead++;newHead++}else if(oldKeys[oldTail]===newKeys[newTail]){// Old tail matches new tail; update in place
newParts[newTail]=updatePart(oldParts[oldTail],newValues[newTail]);oldTail--;newTail--}else if(oldKeys[oldHead]===newKeys[newTail]){// Old head matches new tail; update and move to new tail
newParts[newTail]=updatePart(oldParts[oldHead],newValues[newTail]);insertPartBefore(containerPart,oldParts[oldHead],newParts[newTail+1]);oldHead++;newTail--}else if(oldKeys[oldTail]===newKeys[newHead]){// Old tail matches new head; update and move to new head
newParts[newHead]=updatePart(oldParts[oldTail],newValues[newHead]);insertPartBefore(containerPart,oldParts[oldTail],oldParts[oldHead]);oldTail--;newHead++}else{if(newKeyToIndexMap===void 0){// Lazily generate key-to-index maps, used for removals &
// moves below
newKeyToIndexMap=generateMap(newKeys,newHead,newTail);oldKeyToIndexMap=generateMap(oldKeys,oldHead,oldTail)}if(!newKeyToIndexMap.has(oldKeys[oldHead])){// Old head is no longer in new list; remove
removePart(oldParts[oldHead]);oldHead++}else if(!newKeyToIndexMap.has(oldKeys[oldTail])){// Old tail is no longer in new list; remove
removePart(oldParts[oldTail]);oldTail--}else{// Any mismatches at this point are due to additions or
// moves; see if we have an old part we can reuse and move
// into place
const oldIndex=oldKeyToIndexMap.get(newKeys[newHead]),oldPart=oldIndex!==void 0?oldParts[oldIndex]:null;if(null===oldPart){// No old part for this value; create a new one and
// insert it
const newPart=createAndInsertPart(containerPart,oldParts[oldHead]);updatePart(newPart,newValues[newHead]);newParts[newHead]=newPart}else{// Reuse old part
newParts[newHead]=updatePart(oldPart,newValues[newHead]);insertPartBefore(containerPart,oldPart,oldParts[oldHead]);// This marks the old part as having been used, so that
// it will be skipped in the first two checks above
oldParts[oldIndex]=null}newHead++}}}// Add parts for any remaining new values
while(newHead<=newTail){// For all remaining additions, we insert before last new
// tail, since old pointers are no longer valid
const newPart=createAndInsertPart(containerPart,newParts[newTail+1]);updatePart(newPart,newValues[newHead]);newParts[newHead++]=newPart}// Remove any remaining unused old parts
while(oldHead<=oldTail){const oldPart=oldParts[oldHead++];if(null!==oldPart){removePart(oldPart)}}// Save order of new parts for next round
partListCache.set(containerPart,newParts);keyListCache.set(containerPart,newKeys)}});var repeat$1={repeat:repeat};store.addReducers({myPost});class matterPost extends connect(store)(PageViewElement){static get properties(){return{_posts:{type:Object}}}constructor(){super();store.dispatch(getPostdata())}static get styles(){return[SharedStyles,css`
      .tablemain table {width: 100%;padding: 1em 1em;margin: 0;}
      .tablemain table tbody tr td {border-top:1px solid #f1f1f1;padding:10px;}
      .tablemain table tbody tr th {border-top:1px solid #f1f1f1;padding:10px;background-color:#f1f1f1;text-align: center;}
      .tablemain table tbody tr:nth-child-last th {border-bottom:1px solid #f1f1f1;;}      
      .table_dd_h table tr th:nth-child(1) {width:200px !important;}
      .table_dd_h table tr th:nth-child(3) {width:100px !important;}
      .table_dd_h table tr td {vertical-align:top;text-align:center;}
      `]}render(){return html`
      <div class="container">
			<div class="row">
				<div class="span12">
            <div class="tablemain table_dd_h">
                <table>
                <tbody>
                <tr><th>Heading</th><th>Discription</th><th>Action</th></tr>
                ${repeat(this._posts,(item,index)=>html`
                <tr>
               <td>${item.matter_name}</td>
                  <td>${item.matter_description}</td>
               <td align="center">
                  <div @click="${e=>this._deletePost(item.id)}" title="delete"  class="img_anchor" ><img src="https://image.flaticon.com/icons/svg/230/230366.svg"></div>
                  <div @click="${e=>this._editPost(item.id)}" title="edit" class="img_anchor"><img src="https://image.flaticon.com/icons/svg/526/526127.svg"></div>
              </td>
         </tr>`)}
               </tbody>
              </table>
            </div>
         </div>
      </div>
    </div>`}_deletePost(id){var result=confirm("Do you want to delete this record!!");console.log("result ",result);if(!0/* ignoreName */ /* skipSlots */==result){let post={id:id};store.dispatch(deletePost(post))}}_editPost(ID){let page="matter",editObj={id:ID};store.dispatch(editPost(editObj));store.dispatch(loadPage(page))}stateChanged(state){this._posts=state.myPost.posts}}window.customElements.define("my-matter-post",matterPost);export{repeat$1 as $repeat,repeat};