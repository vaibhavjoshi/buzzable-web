/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { html } from 'lit-element';
import { PageViewElement } from './page-view-element.js';

// These are the shared styles needed by this element.
import { SharedStyles } from './shared-styles.js';
import { store } from '../store.js';
import {  getPostdata } from '../actions/myMetter';
import { connect } from 'pwa-helpers/connect-mixin.js';
import  myPost  from '../reducers/myMetter'; 

store.addReducers({
	myPost
  })

class MyView1 extends connect(store)(PageViewElement) {
  static get styles() {
    return [
      SharedStyles
    ]
  }

render() {
  this._getPost()
    return html`
      <section class="dash_board_main">
        <div class="dash_board_main_flex">
          <div class="main_flex mf-1">
            <div class="main-flex-img">
              <img src="https://image.flaticon.com/icons/svg/145/145867.svg" />
            </div>  
            <div class="">
              <h4>User</h4>
              <p>95</p>
            </div>
          </div>
          <div class="main_flex mf-2">
          <div class="main-flex-img">
          <img src="https://image.flaticon.com/icons/svg/1102/1102457.svg" />
          
        </div>  
        <div class="">
        <h4>Matters</h4>
        <p>52</p>
    </div>
        </div>
          <div class="main_flex mf-3">
          
          <div class="main-flex-img">
          <img src="https://image.flaticon.com/icons/svg/1231/1231594.svg" />
          
        </div>  
        <div class="">
        <h4>Count</h4>
            <p>152</p>
    </div>
         </div>
      </div>
      </section>
    `;
  }

  _getPost(){
      store.dispatch(getPostdata()); 
      console.log("this._deletePost()")
  }
}

window.customElements.define('my-view1', MyView1);
