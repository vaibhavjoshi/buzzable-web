/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { css } from 'lit-element';

export const SharedStyles = css`
  :host {
    display: block;
    box-sizing: border-box;
  }

  .dash_board_main {background-color:#f1f1f1;    min-height: 100vh;}
  .dash_board_main_flex {display:flex;align-items:center;    justify-content: space-around;max-width:100%;}
  .dash_board_main_flex .main_flex {background-color:#fff;border:1px solid #ddd;border-radius:8px;padding:1em 0;text-align:center;width: 200px;
    max-width: 100%;    display: flex;
    justify-content: space-around;
    align-items: center;}

    .dash_board_main_flex .main_flex .main-flex-img {width:50px;height:50px;}
    .dash_board_main_flex .main_flex .main-flex-img img {width:100%;height:100%;}

  .dash_board_main_flex .main_flex h4 {font-weight: 100;font-size: 15px;text-transform: uppercase;color:#000;margin:0;}
  .dash_board_main_flex .main_flex p {color:#000;font-size: 18px;font-weight: 700;margin:0;}
 
  .img_anchor {width:20px;height:20px;display:inline-block;}
  .img_anchor img {width:100%;height:100%;}


  section {
    padding: 24px;
    background: var(--app-section-odd-color);
  }

  section > * {
    max-width: 600px;
    margin-right: auto;
    margin-left: auto;
  }

  section:nth-of-type(even) {
    background: var(--app-section-even-color);
  }

  h2 {
    font-size: 24px;
    text-align: center;
    color: var(--app-dark-text-color);
  }

  @media (min-width: 460px) {
    h2 {
      font-size: 36px;
    }
  }

  .circle {
    display: block;
    width: 64px;
    height: 64px;
    margin: 0 auto;
    text-align: center;
    border-radius: 50%;
    background: var(--app-primary-color);
    color: var(--app-light-text-color);
    font-size: 30px;
    line-height: 64px;
  }
`;
