/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { html, css,LitElement } from 'lit-element';
import { PageViewElement } from './page-view-element.js';
import { store } from '../store.js';
// These are the shared styles needed by this element.
import { SharedStyles } from './shared-styles.js';
import  myPost  from '../reducers/myMetter';

import { repeat } from 'lit-html/directives/repeat.js';
import {  editPost, deletePost, publishPost, getPostdata } from '../actions/myMetter';
//import {  getPostdata,editPost } from '../actions/myMetter'
import { connect } from 'pwa-helpers/connect-mixin.js';
import { loadPage } from '../actions/app'
import { getSelectedResponse } from '../actions/myResponse.js'
import myResponse from '../reducers/myResponseMedia'

store.addReducers({
  myPost,
  myResponse
  })

class matterPost extends connect(store)(PageViewElement) {
	static get properties() {
   return {
      _posts: { type: Object }
    }
  }

constructor(){
  super()
  store.dispatch(getPostdata()) 
} 


static get styles() {
		return [
		  SharedStyles,
      css`
      .tablemain table {width: 100%;padding: 1em 1em;margin: 0;}
      .tablemain table tbody tr td {border-top:1px solid #f1f1f1;padding:10px;}
      .tablemain table tbody tr th {border-top:1px solid #f1f1f1;padding:10px;background-color:#f1f1f1;text-align: center;}
      .tablemain table tbody tr:nth-child-last th {border-bottom:1px solid #f1f1f1;;}      
      .table_dd_h table tr th {width:1% !important;}
      .table_dd_h table tr td {vertical-align:top;text-align:center;}
      .img_anchor {margin:0 5px;}  
      `
		];
  }
       
render() {
  return html`
      <div class="container">
			<div class="row">
				<div class="span12">
            <div class="tablemain table_dd_h">
                <table>
                <tbody>
                <tr><th>Heading</th><th>URL</th><th>Discription</th><th>Action</th></tr>
                ${repeat(this._posts, (item,index) => html`
                <tr>
               <td>${item.matter_name}</td>
               <td>${item.matter_url}</td>
                  <td>${item.matter_description}</td>
               <td align="center">
                  <div @click="${(e) => this._deletePost(item.id)}" title="delete"  class="img_anchor" ><img src="https://image.flaticon.com/icons/svg/230/230366.svg"></div>
                  <div @click="${(e) => this._editPost(item.id)}" title="edit" class="img_anchor"><img src="https://image.flaticon.com/icons/svg/526/526127.svg"></div>
                  <div @click="${(e) => this._viewResponses(item.id,item.matter_name,item.matter_url)}" title="edit" class="img_anchor"><img src="https://image.flaticon.com/icons/svg/593/593516.svg"></div>
                </td>
         </tr>`)}
               </tbody>
              </table>
            </div>
         </div>
      </div>
    </div>`
 }

_deletePost(id){
 var result = confirm("Do you want to delete this record!!"); 
  if(result == true){
   let post = {
    id : id
  }
    store.dispatch(deletePost(post)) 
  } 
}

  _editPost(ID){
    let page = 'matter'
    let editObj ={
        id : ID
    }
   store.dispatch(editPost(editObj))
   store.dispatch(loadPage(page))
  }

  _viewResponses(ID,metter_name,matter_url){
        let page = 'responselist'
        let editObj ={
            "matter_url" : matter_url,
            "metter_name" : metter_name, 
            "id" : ID
      }
     store.dispatch(loadPage(page))
     store.dispatch(getSelectedResponse(editObj)) 
  }
 
  stateChanged(state) {
    this._posts = state.myPost.posts 
   }
} 

window.customElements.define('my-matter-post', matterPost);
