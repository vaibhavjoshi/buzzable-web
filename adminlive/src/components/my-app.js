/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { LitElement, html, css } from 'lit-element';
import { setPassiveTouchGestures } from '@polymer/polymer/lib/utils/settings.js';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { installMediaQueryWatcher } from 'pwa-helpers/media-query.js';
import { installOfflineWatcher } from 'pwa-helpers/network.js';
import { installRouter } from 'pwa-helpers/router.js';
import { updateMetadata } from 'pwa-helpers/metadata.js';

// This element is connected to the Redux store.
import { store } from '../store.js';

// These are the actions needed by this element.
import {
  navigate,
  updateOffline,
  updateDrawerState,
  updateLayout
} from '../actions/app.js';
import { loginAction,forgetPassword } from '../actions/login.js';

// These are the elements needed by this element.
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-scroll-effects/effects/waterfall.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import { menuIcon } from './my-icons.js';
import './snack-bar.js';

class MyApp extends connect(store)(LitElement) {
  static get properties() {
    return {
      appTitle: { type: String },
      _page: { type: String },
      _drawerOpened: { type: Boolean },
      _snackbarOpened: { type: Boolean },
      _offline: { type: Boolean },
      _wideLayout: { type: Boolean }
    };
  }

  static get styles() {
    return [
      css`
        :host {
          --app-drawer-width: 256px;
          display: block;

          --app-primary-color: #E91E63;
          --app-secondary-color: #293237;
          --app-dark-text-color: var(--app-secondary-color);
          --app-light-text-color: white;
          --app-section-even-color: #f7f7f7;
          --app-section-odd-color: white;

          --app-header-background-color: white;
          --app-header-text-color: var(--app-dark-text-color);
          --app-header-selected-color: var(--app-primary-color);

          --app-drawer-background-color: var(--app-secondary-color);
          --app-drawer-text-color: var(--app-light-text-color);
          --app-drawer-selected-color: #78909C;
        }

        app-header {
          position: fixed;
          top: 0;
          left: 0;
          right: 0;
          text-align: center;
          background-color: var(--app-header-background-color);
          color: var(--app-header-text-color);
          border-bottom: 1px solid #eee;
          text-align:left;
        }

        .toolbar-top {
          background-color: var(--app-header-background-color);
        }

        [main-title] {
          font-family: 'Pacifico';
          text-transform: lowercase;
          font-size: 30px;
          margin-right: 44px;
        }

        .menu-btn {
          background: none;
          border: none;
          fill: var(--app-header-text-color);
          cursor: pointer;
          height: 44px;
          width: 44px;
        }

        .drawer-list {
          box-sizing: border-box;
          width: 100%;
          height: 100%;
          padding: 24px;
          background: var(--app-drawer-background-color);
          position: relative;
        }

        .drawer-list > a {
          display: block;
          text-decoration: none;
          color: var(--app-drawer-text-color);
          line-height: 40px;
          padding: 0 24px;
        }

        .drawer-list > a[selected] {
          color: var(--app-drawer-selected-color);
        }

        /* Workaround for IE11 displaying <main> as inline */
        main {
          display: block;
        }

        .main-content {
          padding-top: 64px;
          min-height: 100vh;
        }

        .page {
          display: none;
        }

        .page[active] {
          display: block;
        }

        footer {
          padding: 24px;
          background: var(--app-drawer-background-color);
          color: var(--app-drawer-text-color);
          text-align: center;
        }

        .d_btn_login {margin: 1em 0;
          display: block;
          text-decoration: none;}
          .d_btn_login .btn-ol {margin: 0 auto;display: block;float: none;}
        /******** login css start **********/
        .wrapper-login{
          background:#3498db;
          margin: 0 auto 0 auto;  
          width:100%; 
          min-height: 100vh;
          display: flex;
          justify-content: center;
          align-items: center;
          text-align: center;
        }
        .box-0{
          background:white;
          width:300px;
          border-radius:6px;
          margin: 0 auto 0 auto;
          padding:0px 0px 70px 0px;
          border: #2980b9 4px solid; 
        }
        
        .email{
          background:#ecf0f1;
          border: #ccc 1px solid;
          border-bottom: #ccc 2px solid;
          padding: 8px;
          width:250px;
          color:#AAAAAA;
          margin-top:10px;
          font-size:1em;
          border-radius:4px;
        }
        
        .password{
          border-radius:4px;
          background:#ecf0f1;
          border: #ccc 1px solid;
          padding: 8px;
          width:250px;
          font-size:1em;
        }
        .title_admin {
        margin: 10px 0 0;
        font-weight: 100;
        font-size: 25px;}
    
        .pb-0 {padding-bottom:0 !important;}
        .btn-ol{
          background:#2ecc71;
          width:125px;
          padding-top:5px;
          padding-bottom:5px;
          color:white;
          border-radius:4px;
          border: #27ae60 1px solid;
          
          margin-top:20px;
          margin-bottom:20px;
          float:left;
          margin-left:16px;
          font-weight:800;
          font-size:0.8em;
        }
        
        .btn-ol:hover{
          background:#2CC06B; 
        }

        .logout-b a {width: 35px;height: 35px;display:inline-block;}
        .logout-b a img {width:100%;height:100%;}
        #btn-ol2{
          float:left;
          background:#3498db;
          width:125px;  padding-top:5px;
          padding-bottom:5px;
          color:white;
          border-radius:4px;
          border: #2980b9 1px solid;
          
          margin-top:20px;
          margin-bottom:20px;
          margin-left:10px;
          font-weight:800;
          font-size:0.8em;
        }
        
        #btn-ol2:hover{ 
        background:#3594D2; 
        }
        /******** login css end ************/

      
        /* Wide layout */
        @media (min-width: 768px) {
          app-header,
          .main-content,
          footer {
            margin-left: var(--app-drawer-width);
          }
          .menu-btn {
            display: none;
          }

          [main-title] {
            margin-right: 0;
          }
        }
      `
    ];
  }
render() {
 const storageSession = JSON.parse(localStorage.getItem('user'));
 
 return  html` 
    ${ storageSession == null ?  html` 
     <div class="wrapper-login">
     <form id="loginForm">
         <div class="box-0 pb-0">
            <h1 class="title_admin">ADMIN</h1>
              <input type="email" class="email" placeholder="Email"/>
              <input type="password" class="email" placeholder="Password"/>
                <a class="d_btn_login" href="" @click="${this._login}"><div class="btn-ol">Sign In</div></a> 
         </div> 
             
      </form>
     </div>`
      : html` <!-- Header -->
     <app-header condenses reveals effects="waterfall">
       <app-toolbar class="toolbar-top">
         <button class="menu-btn" title="Menu" @click="${this._menuButtonClicked}">${menuIcon}</button>
         <div main-title>${this.appTitle}</div>
        <div class="logout-b">
          <a href="#" @click="${this._logout}"><img src="../../images/logout.png" /></a>
        </div>

       </app-toolbar>
     </app-header>

     <!-- Drawer content -->
     <app-drawer .opened="${this._drawerOpened}" .persistent="${this._wideLayout}"
         @opened-changed="${this._drawerOpenedChanged}">
       <nav class="drawer-list">
         <a ?selected="${this._page === 'matter'}" href="/matter">Create Matter</a>
         <a ?selected="${this._page === 'matterpost'}" href="/matterpost">Matter List</a>
         <a ?selected="${this._page === 'responselist'}" href="/responselist">Response List</a>
        
       </nav>
     </app-drawer>

     <!-- Main content -->
     <main role="main" class="main-content">
       <my-view1 class="page" ?active="${this._page === 'view1'}"></my-view1>
       <my-view2 class="page" ?active="${this._page === 'view2'}"></my-view2>
       <my-view3 class="page" ?active="${this._page === 'view3'}"></my-view3>
       <my-matter class="page" ?active="${this._page === 'matter'}"></my-matter>
       <my-matter-post class="page" ?active="${this._page === 'matterpost'}"></my-matter-post>
       <my-matter class="page" ?active="${this._page === 'editpost'}"></my-matter>
       <my-response-list class="page" ?active="${this._page === 'responselist'}"></my-response-list>
       <my-view404 class="page" ?active="${this._page === 'view404'}"></my-view404>
     </main>

     <footer>
       <p>Made with &hearts; by the Polymer team.</p>
     </footer>

     <snack-bar ?active="${this._snackbarOpened}">
       You are now ${this._offline ? 'offline' : 'online'}.
     </snack-bar> 
   ` }` 

    // Anything that's related to rendering should be done in here.
    /* return html`

      <!-- Header -->
      <app-header condenses reveals effects="waterfall">
        <app-toolbar class="toolbar-top">
          <button class="menu-btn" title="Menu" @click="${this._menuButtonClicked}">${menuIcon}</button>
          <div main-title>${this.appTitle}</div>
        </app-toolbar>
      </app-header>

      <!-- Drawer content -->
      <app-drawer .opened="${this._drawerOpened}" .persistent="${this._wideLayout}"
          @opened-changed="${this._drawerOpenedChanged}">
        <nav class="drawer-list">
          <a ?selected="${this._page === 'view1'}" href="/view1">Dashboard</a>
          <a ?selected="${this._page === 'matter'}" href="/matter">Create Matter</a>
          <a ?selected="${this._page === 'matterpost'}" href="/matterpost">Matter List</a>
          <a ?selected="${this._page === 'login'}" href="/login">Login</a>
        </nav>
      </app-drawer>

      <!-- Main content -->
      <main role="main" class="main-content">
        <my-view1 class="page" ?active="${this._page === 'view1'}"></my-view1>
        <my-view2 class="page" ?active="${this._page === 'view2'}"></my-view2>
        <my-view3 class="page" ?active="${this._page === 'view3'}"></my-view3>
        <my-matter class="page" ?active="${this._page === 'matter'}"></my-matter>
        <my-matter-post class="page" ?active="${this._page === 'matterpost'}"></my-matter-post>
        <my-login class="page" ?active="${this._page === 'login'}"></my-login>
        <my-view404 class="page" ?active="${this._page === 'view404'}"></my-view404>
      </main>

      <footer>
        <p>Made with &hearts; by the Polymer team.</p>
      </footer>

      <snack-bar ?active="${this._snackbarOpened}">
        You are now ${this._offline ? 'offline' : 'online'}.
      </snack-bar> 
    `; */
  }

  constructor() {
    super();
    // To force all event listeners for gestures to be passive.
    // See https://www.polymer-project.org/3.0/docs/devguide/settings#setting-passive-touch-gestures
    setPassiveTouchGestures(true);
  }

  firstUpdated() {
    installRouter((location) => store.dispatch(navigate(decodeURIComponent(location.pathname))));
    installOfflineWatcher((offline) => store.dispatch(updateOffline(offline)));
    installMediaQueryWatcher(`(min-width: 768px)`,
        (matches) => store.dispatch(updateLayout(matches)));
  }

  updated(changedProps) {
    if (changedProps.has('_page')) {
      const pageTitle = this.appTitle + ' - ' + this._page;
      updateMetadata({
        title: pageTitle,
        description: pageTitle
        // This object also takes an image property, that points to an img src.
      });
    }
  }

  _menuButtonClicked() {
    store.dispatch(updateDrawerState(true));
  }

  _drawerOpenedChanged(e) {
    store.dispatch(updateDrawerState(e.target.opened));
  }

  stateChanged(state) {
    this._page = state.app.page;
    this._offline = state.app.offline;
    this._snackbarOpened = state.app.snackbarOpened;
    this._drawerOpened = state.app.drawerOpened;
    this._wideLayout = state.app.wideLayout;
  }

  _login(){
    const formData = this.shadowRoot.querySelector('#loginForm')
   
    const email = formData.elements[0].value
    const password = formData.elements[1].value
      
     const userInfo ={
       "email" : email,
       "password" : password
     }
     store.dispatch(loginAction(userInfo))
  }

  _logout(){
    localStorage.removeItem("user")
    location.reload();
  }

  _forgetPassword(){
       store.dispatch(forgetPassword())
  }
}

window.customElements.define('my-app', MyApp);
