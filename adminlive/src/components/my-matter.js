/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { html, css,LitElement } from 'lit-element';
import { PageViewElement } from './page-view-element.js';
import { store } from '../store.js';
// These are the shared styles needed by this element.
import { SharedStyles } from './shared-styles.js';
import { postData } from '../actions/myMetter';
import  myPost  from '../reducers/myMetter'; 
import  '@polymer/polymer'
import '@polymer/paper-button'
import '@polymer/iron-form'
import { connect } from 'pwa-helpers/connect-mixin.js';
import {  getPostdata, editPostRequest } from '../actions/myMetter';
import { loadPage } from '../actions/app'


store.addReducers({
	myPost
  })

class MyMatter extends connect(store)(LitElement) {
     static get properties() {
		return {
		   heading: { type: String},
		   discription: { type: String},
		   matterId : {type: Number}   
		 }
	   }
	
	static get styles() {
		return [
		  SharedStyles,
		  css`
			*{box-sizing: border-box;-webkit-box-sizing: border-box;}
			.fieldss {max-width: 500px;margin: 0 auto;}
			.legend-f {font-size: 22px;padding: 0 0 10px;}
			.fieldss {padding: 1em 1em;}
			.fieldss input {padding:0 1em;height: 40px;width: 100%;border: 1px solid #ddd;box-shadow: none;margin: 7px 0 0;}
			.fieldss textarea {padding:1em 1em;height: 150px;width: 100%;border: 1px solid #ddd;box-shadow: none;margin: 7px 0 0;}
			.fieldss button {cursor:pointer;background-color: #293237;box-shadow: none;border: none;width: 150px;height: 40px;font-size: 18px;border-radius: 5px;color: #fff;}
			.fieldss button:hover {background-color:#121212;}
			.fieldss.ft-0 {padding-top:0;}
			`
		]
	  }
  
  render() {
  return html`
	${ this.heading ?  html`
	<form class="fieldss" id="checkoutForm">
			<div class="legend-f">
			<h3>Matters Post</h3>
			  <div>
				<div class="form-group">
					<input type="text" id="metter" .value=${this.heading} class="form-control" placeholder="Matters title" />
				</div>
				<div class="form-group">
					<textarea type="text" class="form-control" placeholder="Matters Description">${this.discription}</textarea>		
				</div>
				<div class="form-group">
					<input type="button" class="btn" @click="${this._editSubmit}" value="POST">
				</div>
			</form> ` : html`
			<form class="fieldss" id="checkoutForm">
					<div class="legend-f">
					<h3>Matters Post</h3>
					  <div>
						<div class="form-group">
							<input type="text" id="metter"  class="form-control" placeholder="Matters title" />
						</div>
						<div class="form-group">
							<textarea type="text" class="form-control" placeholder="Matters Description"></textarea>		
						</div>
						<div class="form-group">
							<input type="button" class="btn" @click="${this._submit}" value="POST">
						</div>
					</form> ` } `
				}

_submit(from) {

		const formData = this.shadowRoot.querySelector('#checkoutForm')
		const textareaElement = formData.getElementsByTagName("textarea");
		const discription = textareaElement[0].value 
		let heading = formData.elements[0].value
		
		var uniqeString = Math.random().toString(36).substr(2, 9)
		let url="https://buzzable-front.firebaseapp.com/?"+"uniqueString=" + uniqeString

		let postDatametter = {
			"heading" : heading,
			"discription" : discription,
			"matter_url" : url
		}

		store.dispatch(postData(postDatametter))
     	let page = 'matterpost'
		store.dispatch(loadPage(page))

		this.shadowRoot.querySelector('#checkoutForm').elements[0].value = ''
    	this.shadowRoot.querySelector('#checkoutForm').getElementsByTagName("textarea")[0].value = ''
   }

_editSubmit() {
    const formData = this.shadowRoot.querySelector('#checkoutForm')
    const textareaElement = formData.getElementsByTagName("textarea");
	const discription = textareaElement[0].value 
	let heading = formData.elements[0].value
	
	let editPostDataMetter = {
		"heading" : heading,
		"discription" : discription,
		"id" : this.matterId
	}

     store.dispatch(editPostRequest(editPostDataMetter))
	 store.dispatch(getPostdata())
	 let page = 'matterpost'
	 store.dispatch(loadPage(page))
	 this.shadowRoot.querySelector('#checkoutForm').elements[0].value = ''
     this.shadowRoot.querySelector('#checkoutForm').getElementsByTagName("textarea")[0].value = ''
	}

  _getPost(){
   store.dispatch(getPostdata())
  }

 stateChanged(state) {
	// console.log("STATE ",state)
	this.heading = state.myPost.editMetter.matter_name
	this.discription = state.myPost.editMetter.matter_description
	this.matterId = state.myPost.editMetter.id
  }
}

window.customElements.define('my-matter', MyMatter);
