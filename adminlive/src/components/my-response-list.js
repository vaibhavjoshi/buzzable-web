/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { html, css,LitElement } from 'lit-element';
import { PageViewElement } from './page-view-element.js';
import { store } from '../store.js';
// These are the shared styles needed by this element.
import { SharedStyles } from './shared-styles.js';
import  myPost  from '../reducers/myMetter'; 
import { repeat } from 'lit-html/directives/repeat.js';
import {  editPost, deletePost, publishPost, getPostdata } from '../actions/myMetter';
//import {  getPostdata,editPost } from '../actions/myMetter'
import { connect } from 'pwa-helpers/connect-mixin.js';
import { loadPage } from '../actions/app'
import myResponse from '../reducers/myResponseMedia' 

store.addReducers({
  myPost,
  myResponse
  })

class ResponseList extends connect(store)(PageViewElement) {
	static get properties() {
   return {
    _responses: { type: Object },
    _matterUrl: {type:String},
    _matterHeading: {type:String}
    }
  }

constructor(){
  super()
} 


static get styles() {
		return [
		  SharedStyles,
      css`
      .tablemain table {width: 100%;padding: 1em 1em;margin: 0;}
      .tablemain table tbody tr td {border-top:1px solid #f1f1f1;padding:10px;}
      .tablemain table tbody tr th {border-top:1px solid #f1f1f1;padding:10px;background-color:#f1f1f1;text-align: center;}
      .tablemain table tbody tr:nth-child-last th {border-bottom:1px solid #f1f1f1;;}      
      .table_dd_h table tr th {width:1% !important;}
      .table_dd_h table tr td {vertical-align:top;text-align:center;}

      .highliting_t {padding: 8px 1em;background: #f1f1f1;margin: 1em 1em 0;    border-radius: 5px;border: 1px solid #ddd;}


      `
		];
  }
       
render() {
  return html`
      <div class="container">
      <div class="row">
      <h4 class="highliting_t">URL :${ this._matterUrl}</h4>
      <h4 class="highliting_t">Heading : ${ this._matterHeading}</h4>
				<div class="span12">
            <div class="tablemain table_dd_h">
                <table>
                <tbody>
                <tr><th>Answer</th><th>Device Number</th><th>Email</th></tr>
                ${repeat(this. _responses, (item,index) => html`
                <tr>
                  <td>${item.media_description}</td>
                  <td>${item.Response.device_id}</td>
                  <td>${item.Response.email_id}</td>
             </tr>`)}
               </tbody>
              </table>
            </div>
         </div>
      </div>
    </div>`
 }

 stateChanged(state) {
  console.log("STATE RESPO.",state)
  this._responses = state.myResponse.response.data
  this._matterUrl = state.myResponse.response.matter.matter_url
  this._matterHeading = state.myResponse.response.matter.metter_name  
 }
} 

window.customElements.define('my-response-list', ResponseList);
