import { SET_USER_RESPONSE_MEDIA } from '../actions/myResponse.js'

const INITIAL_STATE = {
    response:{}
  }

const myResponse = (state = INITIAL_STATE, action) => {
  
  switch (action.type) {
    case SET_USER_RESPONSE_MEDIA:
       
      return {
        ...state,
        response : action.data
       }
    default:
      return state;
  }
}

export default myResponse


