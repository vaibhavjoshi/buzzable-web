import {  POST_REQUEST,SET_POSTS,TEST_STATE,SET_EDIT_MATTER,REFRESH_STATE } from '../actions/myMetter.js'

const INITIAL_STATE = {
    posts:{},
    postData : {},
    error: '',
    editMetter : {},
    status : {}
  }

const myPost = (state = INITIAL_STATE, action) => {
  switch (action.type) {
   case POST_REQUEST:
      return {
        ...state,
        posts : action.data
       }

    case SET_POSTS:
     return {
        ...state,
        posts : action.data ? action.data : action.data
       }
    case TEST_STATE:
      return {
        ...state,
        posts : action.data
      }
    case SET_EDIT_MATTER:
      return {
        ...state,
        editMetter : action.data
      } 
    case REFRESH_STATE:
      return {
        ...state,
        status : action.data
      }       
    default:
      return state;
  }
  
}

export default myPost


