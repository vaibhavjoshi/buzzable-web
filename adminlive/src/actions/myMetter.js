export const POST_REQUEST = 'POST_REQUEST'
export const SET_POSTS = 'SET_POSTS'
export const TEST_STATE = 'TEST_STATE'
export const SET_EDIT_MATTER = 'SET_EDIT_MATTER'
export const REFRESH_STATE = 'REFRESH_STATE'

import {axios} from '@bundled-es-modules/axios';
//import { toastr } from "react-redux-toastr";
import '@polymer/paper-toast/paper-toast.js';
import {config} from '../lib/config.js' 

export const postData = (post) => (dispatch) => {
   axios.post(config.API_BASE_URL+'/api/metter_post', post)
   .then(response => {
     let postMetterObj = {
        matter_description : response.data.data.matter_description,
        matter_name : response.data.data.matter_name,
        id : response.data.data.id
      }
      dispatch({
        type: POST_REQUEST,
        data :response.data.data
      }) 
      
  })
    .catch(error => {
      const errorData = error.response ? error.response.data : error;
      dispatch(failure(errorData.message))
    }) 
  }

  export const editPostRequest = (post) => (dispatch) => {
     axios.post(config.API_BASE_URL+'/api/metter_post_edit_request', post)
       .then(response => {
          dispatch({
            type: POST_REQUEST,
            data :response.data.data
          }) 
       })
        .catch(error => {
          const errorData = error.response ? error.response.data : error;
          dispatch(failure(errorData.message))
        }) 
      }

  export const getPostdata = () => (dispatch) => {
    axios.get(config.API_BASE_URL+'/api/get_metter')
      .then(response => {
        dispatch({
                  type: SET_POSTS,
                  data :response.data.data
                })
          })

        .catch(error => {
         
          const errorData = error.response ? error.response.data : error
          dispatch(failure(errorData.message))
        })
      }
  
  export const deletePost = (post) => (dispatch) => {
   axios.post(config.API_BASE_URL+'/api/delete_metter',post)
   .then(response => {   
        dispatch({
          type: TEST_STATE,
          data :response.data.data
          })
      })

    .catch(error => {
       
          const errorData = error.response ? error.response.data : error
          dispatch(failure(errorData.message))
        })
    }

 export const editPost = (post) => (dispatch) => {
    axios.post(config.API_BASE_URL+'/api/get_edit_metter',post)
       .then(response => {
             dispatch({
                    type: SET_EDIT_MATTER,
                    data :response.data.data
                  }) 
            })
  
          .catch(error => {
            const errorData = error.response ? error.response.data : error
            dispatch(failure(errorData.message))
          })
        }

  export const publishPost = () => (dispatch) => {
    console.log("publishPost action")
  }



  







