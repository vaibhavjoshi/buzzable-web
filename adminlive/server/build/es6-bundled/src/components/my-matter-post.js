define(["exports","./my-app.js"],function(_exports,_myApp){"use strict";Object.defineProperty(_exports,"__esModule",{value:!0});_exports.repeat=_exports.$repeat=void 0;const createAndInsertPart=(containerPart,beforePart)=>{const container=containerPart.startNode.parentNode,beforeNode=beforePart===void 0?containerPart.endNode:beforePart.startNode,startNode=container.insertBefore((0,_myApp.createMarker)(),beforeNode);container.insertBefore((0,_myApp.createMarker)(),beforeNode);const newPart=new _myApp.NodePart(containerPart.options);newPart.insertAfterNode(startNode);return newPart},updatePart=(part,value)=>{part.setValue(value);part.commit();return part},insertPartBefore=(containerPart,part,ref)=>{const container=containerPart.startNode.parentNode,beforeNode=ref?ref.startNode:containerPart.endNode,endNode=part.endNode.nextSibling;if(endNode!==beforeNode){(0,_myApp.reparentNodes)(container,part.startNode,endNode,beforeNode)}},removePart=part=>{(0,_myApp.removeNodes)(part.startNode.parentNode,part.startNode,part.endNode.nextSibling)},generateMap=(list,start,end)=>{const map=new Map;for(let i=start;i<=end;i++){map.set(list[i],i)}return map},partListCache=new WeakMap,keyListCache=new WeakMap,repeat=(0,_myApp.directive)((items,keyFnOrTemplate,template)=>{let keyFn;if(template===void 0){template=keyFnOrTemplate}else if(keyFnOrTemplate!==void 0){keyFn=keyFnOrTemplate}return containerPart=>{if(!(containerPart instanceof _myApp.NodePart)){throw new Error("repeat can only be used in text bindings")}const oldParts=partListCache.get(containerPart)||[],oldKeys=keyListCache.get(containerPart)||[],newParts=[],newValues=[],newKeys=[];let index=0;for(const item of items){newKeys[index]=keyFn?keyFn(item,index):index;newValues[index]=template(item,index);index++}let newKeyToIndexMap,oldKeyToIndexMap,oldHead=0,oldTail=oldParts.length-1,newHead=0,newTail=newValues.length-1;while(oldHead<=oldTail&&newHead<=newTail){if(null===oldParts[oldHead]){oldHead++}else if(null===oldParts[oldTail]){oldTail--}else if(oldKeys[oldHead]===newKeys[newHead]){newParts[newHead]=updatePart(oldParts[oldHead],newValues[newHead]);oldHead++;newHead++}else if(oldKeys[oldTail]===newKeys[newTail]){newParts[newTail]=updatePart(oldParts[oldTail],newValues[newTail]);oldTail--;newTail--}else if(oldKeys[oldHead]===newKeys[newTail]){newParts[newTail]=updatePart(oldParts[oldHead],newValues[newTail]);insertPartBefore(containerPart,oldParts[oldHead],newParts[newTail+1]);oldHead++;newTail--}else if(oldKeys[oldTail]===newKeys[newHead]){newParts[newHead]=updatePart(oldParts[oldTail],newValues[newHead]);insertPartBefore(containerPart,oldParts[oldTail],oldParts[oldHead]);oldTail--;newHead++}else{if(newKeyToIndexMap===void 0){newKeyToIndexMap=generateMap(newKeys,newHead,newTail);oldKeyToIndexMap=generateMap(oldKeys,oldHead,oldTail)}if(!newKeyToIndexMap.has(oldKeys[oldHead])){removePart(oldParts[oldHead]);oldHead++}else if(!newKeyToIndexMap.has(oldKeys[oldTail])){removePart(oldParts[oldTail]);oldTail--}else{const oldIndex=oldKeyToIndexMap.get(newKeys[newHead]),oldPart=oldIndex!==void 0?oldParts[oldIndex]:null;if(null===oldPart){const newPart=createAndInsertPart(containerPart,oldParts[oldHead]);updatePart(newPart,newValues[newHead]);newParts[newHead]=newPart}else{newParts[newHead]=updatePart(oldPart,newValues[newHead]);insertPartBefore(containerPart,oldPart,oldParts[oldHead]);oldParts[oldIndex]=null}newHead++}}}while(newHead<=newTail){const newPart=createAndInsertPart(containerPart,newParts[newTail+1]);updatePart(newPart,newValues[newHead]);newParts[newHead++]=newPart}while(oldHead<=oldTail){const oldPart=oldParts[oldHead++];if(null!==oldPart){removePart(oldPart)}}partListCache.set(containerPart,newParts);keyListCache.set(containerPart,newKeys)}});_exports.repeat=repeat;var repeat$1={repeat:repeat};_exports.$repeat=repeat$1;_myApp.store.addReducers({myPost:_myApp.$myMetterDefault});class matterPost extends _myApp.PageViewElement{static get properties(){return{_posts:{type:Object}}}static get styles(){return[_myApp.SharedStyles,_myApp.css`
      .tablemain table {width: 100%;padding: 1em 1em;margin: 0;}
      .tablemain table tbody tr td {border-top:1px solid #f1f1f1;padding:10px;}
      .tablemain table tbody tr th {border-top:1px solid #f1f1f1;padding:10px;background-color:#f1f1f1;text-align: center;}
      .tablemain table tbody tr:nth-child-last th {border-bottom:1px solid #f1f1f1;;}      
      `]}render(){const Obj=_myApp.store.getState();console.log("Obj ",Obj);return _myApp.html`
      <div class="container">
			<div class="row">
				<div class="span12">
            <div class="tablemain">
                <table>
                <tbody>
                <tr><th>Heading</th><th>Discription</th><th>Action</th></tr>
                ${repeat(Obj.myPost.posts,item=>_myApp.html`
                <tr>
                    <td>${item.title}</td>
                     <td>${item.discription}</td>
                     <td>
                        <a title="delete" class="img_anchor" href="javascript:void(0);"><img src="https://image.flaticon.com/icons/svg/230/230366.svg" /></a>
                        <a title="edit" class="img_anchor" href="javascript:void(0);"><img src="https://image.flaticon.com/icons/svg/526/526127.svg" /></a>
                        <a title="publish" class="img_anchor" href="javascript:void(0);"><img src="https://image.flaticon.com/icons/svg/1632/1632748.svg" /></a>
                    </td>
                </tr>
              `)}
               </tbody>
              </table>
            </div>
         </div>
			</div>
    </div>`}}window.customElements.define("my-matter-post",matterPost)});