import { SET_CURRENT_RESPONSE_ID,SET_TEXT_RESPONSE,SET_IMAGE_RESPONSE,SET_AUDIO_RESPONSE,SET_VIDEO_RESPONSE } from '../actions/response.js';

const INITIAL_STATE = {
  response_id: "",
  textResponse : "",
  imageResponse: "",
  videoResponse: "",
  audioResponse: ""
};

const response = (state = INITIAL_STATE, action) => {
  console.log("eeeeeeeee ",action.data)
  switch (action.type) {
    
    case SET_CURRENT_RESPONSE_ID:
      console.log("in get ID")
      return {
        ...state,
        response_id : action.data.id   
     }
    case SET_TEXT_RESPONSE:
      console.log("SET_TEXT_RESPONSE",action.data)
      return {
        ...state,
        textResponse : action.data 
     }

     case SET_IMAGE_RESPONSE:
      console.log("SET_IMAGE_RESPONSE",action.data)
      return {
        ...state,
        imageResponse : action.data 
     }

     case SET_VIDEO_RESPONSE:
      console.log("SET_IMAGE_RESPONSE",action.data)
      return {
        ...state,
        videoResponse : action.data 
     }

     case SET_AUDIO_RESPONSE:
      console.log("SET_IMAGE_RESPONSE",action.data)
      return {
        ...state,
        audioResponse : action.data 
     }

    default:
      return state;
  }
}

export default response;
