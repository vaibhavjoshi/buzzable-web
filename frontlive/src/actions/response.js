export const SET_DISCRIPTION = 'SET_DISCRIPTION'
export const SET_CURRENT_RESPONSE_ID = 'SET_CURRENT_RESPONSE_ID'
export const SET_TEXT_RESPONSE = 'SET_TEXT_RESPONSE'
export const SET_IMAGE_RESPONSE = "SET_IMAGE_RESPONSE"
export const SET_VIDEO_RESPONSE = "SET_VIDEO_RESPONSE"
export const SET_AUDIO_RESPONSE = "SET_AUDIO_RESPONSE"

import {axios} from '@bundled-es-modules/axios'
import {config} from '../lib/config.js'
import { store } from '../store.js';
export const insertResponse = (post) => (dispatch) => {
    
  axios.post(config.API_BASE_URL+'/api/post_response',post)		
	     .then(response => {
		       console.log("responseeeer ",response.data.data.media_description)
          dispatch({
            type: SET_TEXT_RESPONSE,
            data :response.data.data.media_description
          })
      })
      .catch(error => {
        console.log("ERROR ",error)
        const errorData = error.response ? error.response.data : error
        toastr.error("Error", errorData.message)
        dispatch(failure(errorData.message))
      }) 
    }

/* Insert email address */
    
  export const insertResponseEmail = (post) => (dispatch) => {
    console.log("POST ",post)
    axios.post(config.API_BASE_URL+'/api/mail',post)
      .then(response => {
        console.log("response E ",response)
        })
      .catch(error => {
          console.log("ERROR ",error)
          const errorData = error.response ? error.response.data : error
          toastr.error("Error", errorData.message)
          dispatch(failure(errorData.message))
       }) 
    }

    export const insertImageResponse = (response) => (dispatch) => {
        console.log("response actionnn  ",response)
        dispatch({
            type: SET_IMAGE_RESPONSE,
            data :response
          })

        const state = store.getState() 
        console.log("state ",state) 
    }

    export const insertAudioResponse = (response) => (dispatch) => {
      console.log("response actionnn  ",response)
      dispatch({
          type: SET_AUDIO_RESPONSE,
          data :response
        })

        const state = store.getState() 
        console.log("state ",state) 
  }

  export const insertVideoResponse = (response) => (dispatch) => {
    console.log("response actionnn  ",response)
    dispatch({
        type: SET_VIDEO_RESPONSE,
        data :response
      })
      
      const state = store.getState() 
      console.log("state ",state) 
}
    
    
   
    



    