export const GET_QUESTION = 'GET_QUESTION'
export const SET_QUESTION = 'SET_QUESTION'
import {axios} from '@bundled-es-modules/axios';
import {config} from '../lib/config.js'
export const getQuestion = (urlObj) => (dispatch) => {
     
  axios.post(config.API_BASE_URL+'/api/get_question',urlObj)
       .then(response => {
           console.log("response ",response)
          console.log("response Ravi.data ",response.data)
          localStorage.setItem("questionData", JSON.stringify(response.data.matter_description))
              const storageSessionData = JSON.parse(localStorage.getItem('questionData'));
          console.log("storageSessionData ",storageSessionData)
              dispatch({
              type: SET_QUESTION,
              data :response.data.matter_description
            })
           })
        .catch(error => {
          console.log("ERROR ",error)
          //const errorData = error.response ? error.response.data : error
          //toastr.error("Error", errorData.message)
          //dispatch(failure(errorData.message))
        })
    }
