import { css,html } from 'lit-element';
import { PageViewElement } from './page-view-element.js';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../store.js';
import { insertResponse } from '../actions/response.js';
import response from '../reducers/response.js'
import question from '../reducers/question.js'
import {MyView3Style} from './view3-styles.js'
//import {  html,PolymerElement } from '@polymer/polymer/polymer-element.js'

store.addReducers({
	response,
	question
 })

class MyView3 extends connect(store)(PageViewElement) {
	
	static get properties() {
		return {
		   question : { type: String }
		}
	}

	static get styles() {
		return [
			MyView3Style
		] 
	 }

render(){
	return html`
   <div id="wrapper">
   <div class="page-wrap responseText-page litePage-bgcolor">
	   <header id="siteHeader" class="topHeader">
		   <div class="container-fluid">
			   <div class="d-flex justify-content-between">
				   <div class="logo-header">
					   <a href="#"><img src="assets/images/logo-header.png" alt="Logo" /></a>
				   </div>
				   <div class="header-btn">
					   <a href="#" class="tag-btn small-tag">Text</a>
				   </div>
			   </div>
		   </div>
	   </header>
	   <div id="main">
		   <div class="container-fluid">
			   <div class="resText-section">
				   <div class="section-title">
					   <h1>${this.question}</h1>
				   </div>
				   <div class="resText-wrap">
					   <form id="discriptionForm" class="resText-form">
						   <div class="form-group">
							   <div class="textarea-wrap">
								   <textarea class="form-control responseText"></textarea>
								</div>
						   </div>
						   <div class="btn-wrap text-center">
							   <a href="/view4">
								   <button @click="${(e) => this._submit()}" type="submit" class="btn button-lite" id="responsePostBtn">Add Response</button>
							   </a>
							   <div class="msg lite-color">Your response won't be sent just yet</div>
						   </div>
					   </form>
				   </div>
			   </div>
			   <!-- /.resText-section -->
		   </div>
	   </div>
	   <!-- /#main -->
	   <footer id="siteFooter" class="setBottom">
		   <div class="ftr-home container-fluid">
			   <h5>Your voice counts</h5>
			   <p class="lite-color">100% anonymous. 100% private.</p>
		   </div>
	   </footer>
   </div>
</div> `;
}

  _submit() {
      console.log("_submit ")
     const formData = this.shadowRoot.querySelector('#discriptionForm')
	 const textareaElement = formData.getElementsByTagName("textarea")
	 const discription = textareaElement[0].value
	 
     let postDiscriptionData = {
			"response_media_id" : "",
			"media_type" : "",
			"media_path" : "",
			"media_size" : "",
			"created_at" : Date.now,
			"media_description" : discription
	  } 
       console.log("SUBMIT")
	   store.dispatch(insertResponse(postDiscriptionData))
    }

	stateChanged(state) {
		console.log("view3 state",state)
		this.question = state.question.question
	}
}

window.customElements.define('my-view3', MyView3);
