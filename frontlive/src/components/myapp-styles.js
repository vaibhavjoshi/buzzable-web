/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { css } from 'lit-element';

export const MyappStyle = css`
  
body{
   margin:0;
 }
 app-header {
   position: fixed;
   top: 0;
   left: 0;
   right: 0;
   text-align: center;
   background-color: var(--app-header-background-color);
   color: var(--app-header-text-color);
   border-bottom: 1px solid #eee;
 }

 .toolbar-top {
   background-color: var(--app-header-background-color);
 }

 [main-title] {
   font-family: 'Pacifico';
   text-transform: lowercase;
   font-size: 30px;
   margin-right: 44px;
 }

 .menu-btn {
   background: none;
   border: none;
   fill: var(--app-header-text-color);
   cursor: pointer;
   height: 44px;
   width: 44px;
 }

 .drawer-list {
   box-sizing: border-box;
   width: 100%;
   height: 100%;
   padding: 24px;
   background: var(--app-drawer-background-color);
   position: relative;
 }

 .drawer-list > a {
   display: block;
   text-decoration: none;
   color: var(--app-drawer-text-color);
   line-height: 40px;
   padding: 0 24px;
 }

 .drawer-list > a[selected] {
   color: var(--app-drawer-selected-color);
 }

 /* Workaround for IE11 displaying <main> as inline */
 main {
   display: block;
 }

 .main-content {
   min-height: 100vh;
 }

 .page {
   display: none;
 }

 .page[active] {
   display: block;
 }

 footer {
   padding: 24px;
   background: var(--app-drawer-background-color);
   color: var(--app-drawer-text-color);
   text-align: center;
 }

 /* Wide layout */
 @media (min-width: 768px) {
   app-header,
   .main-content,
   footer {
     margin-left: var(--app-drawer-width);
   }
   .menu-btn {
     display: none;
   }

   [main-title] {
     margin-right: 0;
   }
 }

`;
