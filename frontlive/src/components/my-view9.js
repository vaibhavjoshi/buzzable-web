/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

//import { html, css } from 'lit-element';
import { PageViewElement } from './page-view-element.js';
import { connect } from 'pwa-helpers/connect-mixin.js';

// This element is connected to the Redux store.
import { store } from '../store.js';

// These are the actions needed by this element.
import { checkout } from '../actions/shop.js';

// We are lazy loading its reducer.
//import shop, { cartQuantitySelector } from '../reducers/shop.js';
/* store.addReducers({
  shop
}) */

// These are the elements needed by this element.
import './shop-products.js';
import './shop-cart.js';

// These are the shared styles needed by this element.
import { SharedStyles } from './shared-styles.js';
import { SharedBootstrap } from './shared-bootstrap.js';

import { ButtonSharedStyles } from './button-shared-styles.js';
import { addToCartIcon } from './my-icons.js';
//import {  html,PolymerElement } from '@polymer/polymer/polymer-element.js'
import { html } from 'lit-element'
import {MyView9Style} from './view9-styles.js'

class MyView9 extends connect(store)(PageViewElement) {
   static get styles() {
    return [
      MyView9Style
    ];
  }

  render() {
    return html`
  <div id="wrapper">
    <div class="page-wrap thankyou-page yellowPage-bgcolor">
       <div id="main">
          <div class="container-fluid">
             <div class="thankyou-section">
                <div class="thankyou-title">
                   <h1>Thank you</h1>
                   <p>Buzz you next time!</p>
                </div>
                <div class="sharebtn-wrap">
                   <a href="#" class="btn share-btn btn-block"><i class="material-icons mr-4">share</i> <span>Shere this buzz with a friend</span></a>
                </div>
                <div class="emilbox-wrap">
                   <h3 class="title">
                      Nice one! We will email you a responce summary.
                   </h3>
                </div>
             </div><!-- /.question-section -->
          </div>
       </div><!-- /#main -->
       <footer id="siteFooter" class="setBottom">
          <div class="ftr-home container-fluid">
            <a href="" >
              <img src="assets/images/short-logo-black.png" alt="Logo">
            </a>
             <h5 class="text-white mt-3">Your voice counts</h5>
             <p>100% anonymous. 100% private.</p>
          </div>
       </footer>
    </div>
 </div>
  `;
  }

  /* _checkoutButtonClicked() {
    store.dispatch(checkout());
  }

  // This is called every time something is updated in the store.
  stateChanged(state) {
    this._quantity = cartQuantitySelector(state);
    this._error = state.shop.error;
  } */
}

window.customElements.define('my-view9', MyView9);
