/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { html } from 'lit-element';
import { PageViewElement } from './page-view-element.js';

// These are the shared styles needed by this element.
import { SharedStyles } from './shared-styles.js';

class MyMatter extends PageViewElement {
  static get styles() {
    return [
      SharedStyles
    ];
  }
  
  static get properties() {
    return {
      logindata: { type: Object },
      myString: { type: String },
      myArray: { type: Array },
      myBool: { type: Boolean }
    };
  }
  
  constructor() {
    super();
    this.logindata = {username:''};
    this.myString = 'Hello World';
    this.myArray = ['an','array','of','test','data'];
    this.myBool = true;
  }
  
  _clickHandler(e) {
    console.log(e);
    console.log(this.logindata);
    return false;
  }
  
   render() {
    return html`
		<div class="container">
			<div class="row">
				<div class="span12">
					<div id="legend">
					  <legend class="">Add Matter</legend>
					</div>
					<div class="control-group">
					  <!-- Username -->
					  <label class="control-label"  for="username">Matter Heading</label>
					  <div class="controls">
						<input type="text" id="username" name="username" value='${this.logindata.username}' class="input-xlarge">
					  </div>
					</div>
					<div class="control-group">
					  <!-- Password-->
					  <label class="control-label" for="password">Matter Description</label>
					  <div class="controls">
						<textarea type="password" id="password" name="password" placeholder="" class="input-xlarge"></textarea>
					  </div>
					</div>
					<div class="control-group">
					  <!-- Button -->
					  <div class="controls">
						<button class="btn btn-success" @click="${this._clickHandler}">Login</button>
					  </div>
					</div>
				</div>
			</div>
		</div>
    `;
  }
}

window.customElements.define('my-matter', MyMatter);
