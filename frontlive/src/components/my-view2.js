import { html } from 'lit-element';
//import {  html,PolymerElement } from '@polymer/polymer'
import { PageViewElement } from './page-view-element.js';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../store.js';
import question from '../reducers/question.js'
import response from '../reducers/response.js'
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/paper-input/paper-input'
import { MyView2Style } from './view2-styles.js';
import { insertImageResponse,insertVideoResponse,insertAudioResponse } from '../actions/response.js'
 store.addReducers({
   question,
   response
}); 

class MyView2 extends connect(store)(PageViewElement) {  
 
  static get properties() {
    return {
       question : { type: String },
       imageFile : {type:Object}
    }
}

static get styles() {
   return [
      MyView2Style
   ] 
}

render(){
      
 return html`
  <h1>${this.imageFile}</h1>
  <div id="wrapper">
    <div class="page-wrap litePage-bgcolor">
        <header id="siteHeader" class="topHeader">
            <div class="container-fluid">
                <div class="logo-header">
                    <a href="#"><img src="images/logo-header.png" alt="Logo" /></a>
                </div>
            </div>
        </header>
        <div id="main">
            <div class="container-fluid">
                <div class="question-section">
                    <div class="section-title ques-head">
                        <h1>${this.question}</h1>
                        <p>Respond via video, photo, audio or text.</p>
                    </div>

                    <div class="question-wrap">
                    <div class="row dflex">
                          <div class="col-3 col-md-3" >
                              <a id="question">
                                  <div class="question-box" >
                                    <div class="icon">
                                    <img src="images/question-video.png" alt="Video Icon" />
                                   </div>
                                    <div class="title">Video</div>
                                  </div>
                                  <!-- video capture -->
                                  <label class="input_ft">
                                    <input type="file" @change="${(evt) => this.funVideoResponse(evt)}"   accept="video/*;capture=camcorder" >
                                    </label>
                              </a>
                          </div>
                            
                            <!-- /.col-6 -->
                            <div class="col-3 col-md-3">
                            <a id="question">
                                <div class="question-box">
                                    <div class="icon">
                                        <img src="images/question-image.png" alt="Image Icon" />
                                    </div>
                                    <div class="title">Image</div>
                                </div>

                                <!-- image capture -->
                                <label class="input_ft">
                                <input type="file" @change="${(evt) => this.funImageResponse(evt)}"  id="fileinput" accept="image/jpeg">
                                </label>
                                </a>
                            </div>
                            <!-- /.col-6 -->
                            <div class="col-3 col-md-3" onClick="audioIconClick()">
                            <a id="question" >
                                <div class="question-box">
                                    <div class="icon">
                                        <img src="images/question-audio.png" alt="Audio Icon" />
                                    </div>
                                    <div class="title">Audio</div>
                                </div>

                                <!-- Audio capture -->
                                <label class="input_ft">
                                <input type="file" @change="${(evt) => this.funAudioResponse(evt)}" accept="audio/*;capture=microphone">
                                </label>
                                </a>
                            </div>
                            <!-- /.col-6 -->
                            
                <div class="col-3 col-md-3">
                <a href="/view3">
									<div class="question-box">
										<div class="icon">
                   		<img src="images/question-text.png" alt="Text Icon" />
										</div>
										<div class="title">Text</div>
                  </div>
                  </a>
								</div>
     <!-- /.col-6 -->
                        </div>
                    </div>
                </div>
                <!-- /.question-section -->
            </div>
        </div>
        <!-- /#main -->
        <footer id="siteFooter" class="setBottom">
            <div class="ftr-home container-fluid">
                <h5>Your voice counts</h5>
                <p class="lite-color">100% anonymous. 100% private.</p>
            </div>
        </footer>
    </div>
</div>`;
  }

  funImageResponse(evt){
    var file = evt.srcElement.files[0]
    console.log(file)
    var reader = new FileReader()

    reader.readAsDataURL(file)
    
    reader.onload = function(e) {
        const uploadedData = e.target.result;
        console.log("uploadedData ",uploadedData)  
        store.dispatch(insertImageResponse(uploadedData))
   }
}

funVideoResponse(evt){
    var file = evt.srcElement.files[0]
    console.log(file)
    var reader = new FileReader()
    reader.readAsDataURL(file)
    
    reader.onload = function() {
    const uploadedData = btoa(reader.result)
    console.log("uploadedData ",uploadedData)  
    store.dispatch(insertVideoResponse(uploadedData))
   }
}

funAudioResponse(evt){
    var file = evt.srcElement.files[0]
    console.log(file)
    var reader = new FileReader()
    reader.readAsDataURL(file)
    
    reader.onload = function() {
    const uploadedData = btoa(reader.result)
    console.log("uploadedData ",uploadedData)  
    store.dispatch(insertAudioResponse(uploadedData))
   }
}

 stateChanged(state) {
       console.log("view 2 state",state)
       this.question = state.question.question
  }
}

window.customElements.define('my-view2', MyView2);
