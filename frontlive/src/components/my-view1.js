import { html } from 'lit-element';
import { PageViewElement } from './page-view-element.js';
import { store } from '../store.js';
import { connect } from 'pwa-helpers/connect-mixin.js';
import question from '../reducers/question.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';
import { MyView1Style } from './view1-styles.js';
import { SharedBootstrap } from './shared-bootstrap.js';
import { getQuestion } from '../actions/question.js';

class MyView1 extends connect(store)(PageViewElement){
	
    constructor(){
		super();
		this._getQuestion()
	}
	
	static get styles() {
		return [
			MyView1Style
		] 
	}
	 
render(){
	return html `
	 <div id="wrapper">
		<div class="page-wrap">
			<div id="main">
			<div class="homepage container-fluid">
				<div class="logo-center">
				 <a href="/view2"><img src="images/logo-c.png" alt="Logo"/></a>
			  </div>
		   </div>
		</div>
	 </div>
  </div>`
}

_getQuestion() {
      var currentURL = window.location
		var URL =   currentURL.href
	    let urlObj= {
			url : URL
		}
      store.dispatch(getQuestion(urlObj))
  }
}

window.customElements.define('my-view1', MyView1);
