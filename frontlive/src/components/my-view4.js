import { html } from 'lit-element';
import { PageViewElement } from './page-view-element.js';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { store } from '../store.js';
import question from '../reducers/question';
import response from '../reducers/response.js';

import './counter-element.js';
import { SharedStyles } from './shared-styles.js';
import { SharedBootstrap } from './shared-bootstrap.js';
import {axios} from '@bundled-es-modules/axios';
import {MyView4Style} from './view4-styles.js'
store.addReducers({
   question,
   response
})
class MyView4 extends connect(store)(PageViewElement) {
  
   static get properties() {
		return {
         question : { type: String },
         textResponse : { type:String },
         imageResponse:{ type: String },
         videoResponse:{ type: String },
         audioResponse:{ type: String }
		}
	}

   static get styles() {
		return [
			MyView4Style
		] 
	}

render() {
	return html`
      <div id="wrapper">
       <div class="page-wrap submitOne-page litePage-bgcolor">
            <header id="siteHeader" class="topHeader">
               <div class="container-fluid">
                  <div class="logo-header">
                     <a href="#"><img src="assets/images/logo-header.png" alt="Logo"/></a>
                  </div>
               </div>
            </header>
            <div id="main">
               <div class="container-fluid">
                  <div class="submitOne-section">
                     <div class="section-title">
                        <h1>${this.question}</h1>
                     </div>
                     <div class="postList-wrap">
                        <div class="post-items post-video">
                        ${ this.videoResponse !== "" ? html`
                        <div class="PstVideo video_add">
                              <!--== ADDED VIDEO START ==-->
                             <video width="100%" controls="" src="${this.videoResponse}"></video>
                              <!--== ADDED VIDEO END ==-->
                           </div>`
                           : html`<div></div>`
                        }   

                        ${ this.imageResponse !== "" ? html`
                           <div class="video_add">
                              <!--== ADDED IMAGE START ==-->
                                 <img  src="${this.imageResponse}" alt="" />
                              <!--== ADDED IMAGE END ==-->
                           </div>`
                           : html`<div> </div>`
                        }   
                        
                        ${ this.audioResponse !== "" ? html`  
                           <div class="PstVideo video_add">
                              <!--== ADDED AUDIO START ==-->
                                <audio style="width:100%;" width="100%" controls="" src="${this.audioResponse}"></audio>
                              <!--== ADDED AUDIO END ==-->
                           </div>`
                           : html` <div> </div>` 
                        } 
                          <div class="pst-footer">
                              <div class="removeVideo lite-color text-right">Remove</div>   
                           </div>
                        </div>
                     </div>
                     <div class="additionalResponse-wrap mb-5">
                     
                     <div class="section-title post-text">
                        <div class="PstText">
                        <p class="m-0">${this.textResponse}</p>
                        </div>
                        </div>
                         <div class="question-wrap">
                        <div class="section-title">
                           <p>Add additional response?</p>
                        </div>
                           <div class="row dflex">
                              <div class="col-3 col-md-3">
                                 <div class="question-box qb-1">
                                    <div class="icon">
                                       <img  src="assets/images/question-video.png" alt="Video Icon"/>
                                    </div>
                                 </div>
                              </div><!-- /.col-6 -->
                              <div class="col-3 col-md-3">
                                 <div class="question-box qb-2">
                                    <div class="icon">
                                       <img src="assets/images/question-image.png" alt="Image Icon"/>
                                    </div>
                                 </div>
                              </div><!-- /.col-6 -->
                              <div class="col-3 col-md-3">
                                 <div class="question-box qb-3">
                                    <div class="icon">
                                       <img src="assets/images/question-audio.png" alt="Audio Icon"/>
                                    </div>
                                 </div>
                              </div><!-- /.col-6 -->
                              <div class="col-3 col-md-3">
                                 <div class="question-box qb-4">
                                    <div class="icon">
                                       <img src="assets/images/question-text.png" alt="Text Icon"/>
                                    </div>
                                 </div>
                              </div><!-- /.col-6 -->
                           </div>
                        </div>
                     </div>
                     <div class="submitResponse">
                        <a href="/view8">
							<button type="button" class="btn button-lite button-primary">Submit Response</button>
						</a>
                     </div>
                  </div><!-- /.question-section -->
               </div>
            </div><!-- /#main -->
            <footer id="siteFooter" class="setBottom">
               <div class="ftr-home container-fluid">
                  <h5>Your voice counts</h5>
                  <p class="lite-color">100% anonymous. 100% private.</p>
               </div>
            </footer>
         </div>
      </div>
    `;
  }
  
   stateChanged(state) {
     console.log("STATE view 4 ",state)
     this.question = state.question.question
     this.textResponse = state.response.textResponse
     this.imageResponse = state.response.imageResponse
     this.videoResponse = state.response.videoResponse
     this.audioResponse = state.response.audioResponse
   } 
}

window.customElements.define('my-view4', MyView4);