/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

//import { html, css } from 'lit-element';
import { PageViewElement } from './page-view-element.js';
import { connect } from 'pwa-helpers/connect-mixin.js';

// This element is connected to the Redux store.
import { store } from '../store.js';

// These are the actions needed by this element.
import { checkout } from '../actions/shop.js';

// We are lazy loading its reducer.
import shop, { cartQuantitySelector } from '../reducers/shop.js';
/*store.addReducers({
  shop
})*/

// These are the elements needed by this element.
import './shop-products.js';
import './shop-cart.js';

// These are the shared styles needed by this element.
import { SharedStyles } from './shared-styles.js';
import { SharedBootstrap } from './shared-bootstrap.js';

import { ButtonSharedStyles } from './button-shared-styles.js';
import { addToCartIcon } from './my-icons.js'
import { insertResponseEmail } from '../actions/response.js'
//import {  html,PolymerElement } from '@polymer/polymer/polymer-element.js'
import {axios} from '@bundled-es-modules/axios';
import {config} from '../lib/config.js'
import {MyView8Style} from './view8-styles.js'
import response from '../reducers/response.js'
import { html } from 'lit-element'

store.addReducers({
   response
}); 

class MyView8 extends connect(store)(PageViewElement) {
   

   
  

  static get properties() {
		return {
         question : { type: String },
         imageResponse: {type: String}
		}
	}

  static get styles() {
    return [
      MyView8Style
    ]
  }

  render() {
   return html`
 <div id="wrapper">
   <div class="page-wrap thankyou-page yellowPage-bgcolor">
      <div id="main">
         <div class="container-fluid">
            <div class="thankyou-section">
               <div class="thankyou-title">
                  <h1>Thank you</h1>
                  <p>Buzz you next time!</p>
               </div>
               <div class="sharebtn-wrap">
                  <a href="#" class="btn share-btn btn-block"><i class="material-icons mr-4">share</i> <span>Shere this buzz with a friend</span></a>
               </div>
               <div class="emilbox-wrap">
                  <h3 class="title">To see a summary of the responses to this buzz, drop in your email and we will send you a report.</h3>
                  <form class="form" id="emailPost">
                     <div class="form-group">
                        <label>Email address</label>
                        <input type="email" name="" class="email-input form-control" />
                     </div>
                     <div class="btn-wrap">
                     <button type="submit" on-click="_submit" class="btn button-lite" id="responsePostBtn">  <a href="/view9" class="btn send-btn btn-trans">
                           Send
                        </a></button>
                     </div>
                     <p class="tagline">Your email will never be shared</p>
                  </form>
               </div>
            </div><!-- /.question-section -->
         </div>
      </div><!-- /#main -->
      <footer id="siteFooter" class="setBottom">
         <div class="ftr-home container-fluid">
            <a  href="">
              <img src="assets/images/short-logo-black.png" alt="Logo">
            </a>
            <h5 class="text-white mt-3">Your voice counts</h5>
            <p>100% anonymous. 100% private.</p>
         </div>
      </footer>
   </div>
</div>
    `;
  }

_submit() {
   const emailForm = this.shadowRoot.querySelector('#emailPost')
   const email = emailForm[0].value
  
   console.log("email ",email)
   const responseId = JSON.parse(localStorage.getItem('responseId'))
   console.log("responseId ",responseId)

   const postDiscriptionData = {
       "matter_id" : responseId,
       "email_id" : email,
       "text" : "Thankyou For your feedback. buzzable Team"
   } 
   store.dispatch(insertResponseEmail(postDiscriptionData))
 }

   stateChanged(state) {
      console.log("STATE of view 8",state)
    //this._responseImage = 
  } 
}

window.customElements.define('my-view8', MyView8);
