define(["./my-app.js"],function(_myApp){"use strict";/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/ //import { html } from 'lit-element';
class MyView4 extends _myApp.PolymerElement{static get properties(){return{question:{type:String},response:{type:String}}}static get template(){return _myApp.html`
      <style>
      .SourcesansPro{
         font-family: 'Source Sans Pro', sans-serif;
      }
      
      /* Import Font (font-family: 'proxima_novasemibold';) */
      @font-face {
         font-family: 'proxima_novasemibold';
         src: url('../fonts/proxima-nova-samibold-webfont.woff2') format('woff2'),
            url('../fonts/proxima-nova-samibold-webfont.woff') format('woff');
         font-weight: normal;
         font-style: normal;
      }
      .proximaSemibold{
         font-family: 'proxima_novasemibold';
      }
      /*========================================
      Theme Name : Buzzable;
      Primary Color : #ffca00;
      Default Text Color : #111111;
      =========================================*/
      
      /*========================================
      Default style
      =========================================*/
      html{
         height: 100%;
      }
      body, #wrapper {
         height: 100%;
         font-family: 'Source Sans Pro', sans-serif;
         font-size: 14px;
         color: #323232;
         font-weight: normal;
         background: #fff;
         font-style: normal;
         margin: 0;
         padding: 0;
         letter-spacing: 0.2px;
         overflow-x: hidden;
      }
      ::-moz-selection {
         color: #fff;
         background: #ffca00;
      }
      ::selection {
         color: #fff;
         background: #ffca00;
      }
      a, a:hover{
         color: #ffca00;
         -webkit-outline: none;
         outline: none;
         text-decoration: none;
         cursor: pointer;
      }
      a:focus{
         color: #ffca00;
         -webkit-outline: none;
         outline: none;
         text-decoration: none;
         cursor: pointer;
      }
      #wrapper, .page-wrap{
         min-height: 100vh;
         position: relative;
      }
      .page-wrap{
         max-width: 1024px;
         margin: 0 auto;
         padding-bottom: 80px;
      }
      .primary-bgcolor{
         background: #70C624;
      }
      .primary-color{
         color: #70C624;
      }
      .default-color{
         color : #111111;
      }
      .lite-color{
         color : #ababab;
      }
      button, button:focus, img, img:focus, input, input:focus, textarea, 
      textarea:focus, select, select:focus, option, option:focus, .btn:focus{
         -webkit-outline: none;
         outline: none;
      }
      img{
         max-width: 100%;
         height: auto;
      }
      h1, h2, h3, h4, h5{
         color: #111;
         font-family: 'Source Sans Pro', sans-serif;
         font-weight: 600;
         font-style: normal;
         letter-spacing: 0px;
      }
      .userselect-none{
         -webkit-user-select: none;
         -moz-user-select: none;
         -ms-user-select: none;
         -o-user-select: none;
         user-select: none;
      }
      .white-text, .white-text h1, .white-text h2, .white-text h3, .white-text h4, 
      .white-text h5, .white-text h6, .white-text p, .white-text a{
         color: #ffffff;
      }
      #siteFooter{
         position: absolute;
         bottom: 0;
         left: 0;
         right: 0;
      }
      .container-fluid{
         padding-right: 20px;
         padding-left: 20px;
      }
      .container-fluid .container-fluid{
         padding-right: 15px;
         padding-left: 15px;
      }
      .litePage-bgcolor{
         background-color: #f7f7f7;
      }
      .yellowPage-bgcolor{
         background-color: #ffca00;
      }
      textarea.form-control:focus, input.form-control:focus{
         box-shadow: none;
      }
      .button-lite{
         width: 100%;
         text-align: center;
         background: #e9e9e9;
         font-size: 18px;
         font-weight: 700;
         padding: 0.45rem 0.75rem;
      }
      .button-primary{
         background: #ffca00;
      }
      /*========================================
      Home Page
      =========================================*/
      .logo-center{
         max-width: 194px;
         margin: 0 auto;
         padding-top: 150px;
      }
      .ftr-home{
         text-align: center;
         padding-bottom: 35px;
      }
      .ftr-home h5 {
         font-size: 23px;
         margin: 0;
      }
      .ftr-home p{
         font-size: 13px;
         margin-bottom: 0;
      }
      /*========================================
      Question Page
      =========================================*/
      .topHeader {
         padding-top: 67px;
         padding-bottom: 25px;
      }
      .section-title.ques-head{
         margin-bottom: 20px;
      }
      .section-title h1{
         font-size: 25px;
      }
      .section-title p {
         font-size: 16px;
         font-weight: 700;
         color: #111;
      }
      .question-box {
         position: relative;
         font-size: 14px;
         color: #ababab;
         height: 154px;
         max-width: 154px;
         padding: 10px;
         border: solid 1px #dadada;
         border-radius: 4px;
         background: #fff;
         margin: 0 auto;
         margin-bottom: 30px;
         text-align: center;
      }
      .question-box .icon {
         position: absolute;
         left: 0;
         right: 0;
         top: 50%;
         -webkit-transform: translateY(-55%);
         -ms-transform: translateY(-55%);
         transform: translateY(-55%);
      
      }
      .question-box .title {
         position: absolute;
         left: 0;
         right: 0;
         bottom: 18px;
      }
      /*========================================
      Response Audio page
      =========================================*/
      .response-audio{
         text-align: center;
      }
      
      /*========================================
      Response Video page
      =========================================*/
      .response-video{
         text-align: center;
      }
      /*========================================
      Response Text Page
      =========================================*/
      .small-tag{
         display: inline-block;
         background: #ffca00;
         color: #000;
         padding: 3px 12px;
         border-radius: 3px;
         font-size: 12px;
         font-weight: bold;
      }
      .resText-section .section-title h1 {
         font-size: 22px;
      }
      .resText-form .form-group {  
         margin-bottom: 9px;
      }
      .resText-form .textarea-wrap {
         position: relative;
      }
      .resText-form .textarea-wrap .msg {
         position: absolute;
         bottom: 8px;
         right: 20px;
         font-style: italic;
         font-size: 13px;
      }
      .resText-form textarea.form-control {
         font-family: 'Source Sans Pro', sans-serif;
         font-weight: 500;
         width: 100%;
         height: 200px;
         border-radius: 3px;
         border: 1px solid #e9e9e9;
         caret-color: #ffca00;
         color: #000;
         padding: 12px 17px;
         padding-bottom: 22px;
      }
      .resText-form .btn-wrap .msg {
         font-size: 13px;
         text-align: center;
         margin-top: 8px;
      }
      /*========================================
      Submit One Page
      =========================================*/
      .postList-wrap{
         margin-top: 30px;
      }
      .postList-wrap .post-items {
         margin-bottom: 34px;
      }
      .pst-footer{
         margin-top: 6px;
      }
      .post-video .PstVideo img{
         width: 100%;
      }
      .post-image .PstImage img{
         width: 100%;
      }
      .post-audio .PstAudio img{
         width: 100%;
      }
      .pst-footer .videoTime{
         font-weight: 800;
         font-size: 13px;
      }
      .pst-footer .removeVideo {
         text-align: right;
         font-weight: 600;
         font-size: 13px;
         cursor: pointer;
      }
      .additionalResponse-wrap .question-wrap > .row{
         margin-right: -5px;
         margin-left: -5px;
      }
      .additionalResponse-wrap .question-wrap > .row > div{
         padding-right: 5px;
         padding-left: 5px;
      }
      .additionalResponse-wrap .question-box{
         height: 76px;
         max-width: 76px;
         margin-bottom: 10px;
      }
      .additionalResponse-wrap .question-box .icon img{
         max-width: 20px;
      }
      .additionalResponse-wrap .question-box .icon img {
         max-width: 42px;
      }
      .additionalResponse-wrap .question-box.qb-3 .icon img {
         max-width: 28px;
      }
      .submitResponse {
         position: fixed;
         bottom: 0;
         left: 0;
         right: 0;
         z-index: 99;
         padding: 20px;
         background: #f7f7f7;
         max-width: 1024px;
         margin: 0 auto;
      }
      .page-wrap.submitOne-page, .page-wrap.submitTwo-page{
         margin-bottom: 50px;
      }
      /*========================================
      Submit Two Page
      =========================================*/
      .post-text .PstText {
         border: solid 1px #dadada;
         border-radius: 4px;
         background: #fff;
         font-size: 20px;
         font-weight: 600;
         line-height: 26px;
         padding: 18px 17px;
      }
      /*========================================
      Thankyou page
      =========================================*/
      .thankyou-title{
         text-align: center;
      }
      .thankyou-title h1 {
         font-size: 50px;
         font-weight: 600;
         color: #fff;
         margin: 0;
         padding-top: 40px;
      }
      .thankyou-title p {
         font-size: 23px;
         font-weight: 600;
         color: #000000;
         margin-bottom: 1.5rem;
      }
      .sharebtn-wrap .share-btn{
         color:#fff;
         vertical-align: middle;
         text-align: left;
         background-color: #000000;
         display: flex;
      }
      .emilbox-wrap{
          
      text-align: center;
          
      margin-top: 2.5rem;
      }
      .emilbox-wrap h3{
          font-size: 1.3rem;
          color: #000;
      }
      .emilbox-wrap form{
          
      margin-top: 1.5em;
      }
      .emilbox-wrap form label{
          display: block;
          text-align:left;
          color: #000;
          margin-bottom: .1rem;
      }
      .emilbox-wrap .form-group {
          margin-bottom: .5rem;
      }
      .email-input {
          height: 44px;
      }
      .email-input:focus {
          border-color: #111;
          box-shadow: 0 0 0 0.2rem rgba(0,0,0,.25);
      }
      .btn.send-btn{
         color: #000;
         display: block;
         text-align: center;
         width: 100%;
         border: 2px solid #fff;
         font-weight: bold;
         font-size: 1.2rem;
         margin-bottom: 16px;
         background: transparent;
      }
      .btn.send-btn:hover{
         color: #fff;
         background: #000;
      }




      .SourcesansPro{
         font-family: 'Source Sans Pro', sans-serif;
      }
      
      /* Import Font (font-family: 'proxima_novasemibold';) */
      @font-face {
         font-family: 'proxima_novasemibold';
         src: url('../fonts/proxima-nova-samibold-webfont.woff2') format('woff2'),
            url('../fonts/proxima-nova-samibold-webfont.woff') format('woff');
         font-weight: normal;
         font-style: normal;
      }
      .proximaSemibold{
         font-family: 'proxima_novasemibold';
      }
      /*========================================
      Theme Name : Buzzable;
      Primary Color : #ffca00;
      Default Text Color : #111111;
      =========================================*/
      
      /*========================================
      Default style
      =========================================*/
      html{
         height: 100%;
      }
      body, #wrapper {
         height: 100%;
         font-family: 'Source Sans Pro', sans-serif;
         font-size: 14px;
         color: #323232;
         font-weight: normal;
         background: #fff;
         font-style: normal;
         margin: 0;
         padding: 0;
         letter-spacing: 0.2px;
         overflow-x: hidden;
      }
      ::-moz-selection {
         color: #fff;
         background: #ffca00;
      }
      ::selection {
         color: #fff;
         background: #ffca00;
      }
      a, a:hover{
         color: #ffca00;
         -webkit-outline: none;
         outline: none;
         text-decoration: none;
         cursor: pointer;
      }
      a:focus{
         color: #ffca00;
         -webkit-outline: none;
         outline: none;
         text-decoration: none;
         cursor: pointer;
      }
      #wrapper, .page-wrap{
         min-height: 100vh;
         position: relative;
      }
      .page-wrap{
         max-width: 1024px;
         margin: 0 auto;
         padding-bottom: 80px;
      }
      .primary-bgcolor{
         background: #70C624;
      }
      .primary-color{
         color: #70C624;
      }
      .default-color{
         color : #111111;
      }
      .lite-color{
         color : #ababab;
      }
      button, button:focus, img, img:focus, input, input:focus, textarea, 
      textarea:focus, select, select:focus, option, option:focus, .btn:focus{
         -webkit-outline: none;
         outline: none;
      }
      img{
         max-width: 100%;
         height: auto;
      }
      h1, h2, h3, h4, h5{
         color: #111;
         font-family: 'Source Sans Pro', sans-serif;
         font-weight: 600;
         font-style: normal;
         letter-spacing: 0px;
      }
      .userselect-none{
         -webkit-user-select: none;
         -moz-user-select: none;
         -ms-user-select: none;
         -o-user-select: none;
         user-select: none;
      }
      .white-text, .white-text h1, .white-text h2, .white-text h3, .white-text h4, 
      .white-text h5, .white-text h6, .white-text p, .white-text a{
         color: #ffffff;
      }
      #siteFooter{
         position: absolute;
         bottom: 0;
         left: 0;
         right: 0;
      }
      .container-fluid{
         padding-right: 20px;
         padding-left: 20px;
      }
      .container-fluid .container-fluid{
         padding-right: 15px;
         padding-left: 15px;
      }
      .litePage-bgcolor{
         background-color: #f7f7f7;
      }
      .yellowPage-bgcolor{
         background-color: #ffca00;
      }
      textarea.form-control:focus, input.form-control:focus{
         box-shadow: none;
      }
      .button-lite{
         width: 100%;
         text-align: center;
         background: #e9e9e9;
         font-size: 18px;
         font-weight: 700;
         padding: 0.45rem 0.75rem;
      }
      .button-primary{
         background: #ffca00;
      }
      /*========================================
      Home Page
      =========================================*/
      .logo-center{
         max-width: 194px;
         margin: 0 auto;
         padding-top: 150px;
      }
      .ftr-home{
         text-align: center;
         padding-bottom: 35px;
      }
      .ftr-home h5 {
         font-size: 23px;
         margin: 0;
      }
      .ftr-home p{
         font-size: 13px;
         margin-bottom: 0;
      }
      /*========================================
      Question Page
      =========================================*/
      .topHeader {
         padding-top: 67px;
         padding-bottom: 25px;
      }
      .section-title.ques-head{
         margin-bottom: 20px;
      }
      .section-title h1{
         font-size: 25px;
      }
      .section-title p {
         font-size: 16px;
         font-weight: 700;
         color: #111;
      }
      .question-box {
         position: relative;
         font-size: 14px;
         color: #ababab;
         height: 154px;
         max-width: 154px;
         padding: 10px;
         border: solid 1px #dadada;
         border-radius: 4px;
         background: #fff;
         margin: 0 auto;
         margin-bottom: 30px;
         text-align: center;
      }
      .question-box .icon {
         position: absolute;
         left: 0;
         right: 0;
         top: 50%;
         -webkit-transform: translateY(-55%);
         -ms-transform: translateY(-55%);
         transform: translateY(-55%);
      
      }
      .question-box .title {
         position: absolute;
         left: 0;
         right: 0;
         bottom: 18px;
      }
      /*========================================
      Response Audio page
      =========================================*/
      .response-audio{
         text-align: center;
      }
      
      /*========================================
      Response Video page
      =========================================*/
      .response-video{
         text-align: center;
      }
      /*========================================
      Response Text Page
      =========================================*/
      .small-tag{
         display: inline-block;
         background: #ffca00;
         color: #000;
         padding: 3px 12px;
         border-radius: 3px;
         font-size: 12px;
         font-weight: bold;
      }
      .resText-section .section-title h1 {
         font-size: 22px;
      }
      .resText-form .form-group {  
         margin-bottom: 9px;
      }
      .resText-form .textarea-wrap {
         position: relative;
      }
      .resText-form .textarea-wrap .msg {
         position: absolute;
         bottom: 8px;
         right: 20px;
         font-style: italic;
         font-size: 13px;
      }
      .resText-form textarea.form-control {
         font-family: 'Source Sans Pro', sans-serif;
         font-weight: 500;
         width: 100%;
         height: 200px;
         border-radius: 3px;
         border: 1px solid #e9e9e9;
         caret-color: #ffca00;
         color: #000;
         padding: 12px 17px;
         padding-bottom: 22px;
      }
      .resText-form .btn-wrap .msg {
         font-size: 13px;
         text-align: center;
         margin-top: 8px;
      }
      /*========================================
      Submit One Page
      =========================================*/
      .postList-wrap{
         margin-top: 30px;
      }
      .postList-wrap .post-items {
         margin-bottom: 34px;
      }
      .pst-footer{
         margin-top: 6px;
      }
      .post-video .PstVideo img{
         width: 100%;
      }
      .post-image .PstImage img{
         width: 100%;
      }
      .post-audio .PstAudio img{
         width: 100%;
      }
      .pst-footer .videoTime{
         font-weight: 800;
         font-size: 13px;
      }
      .pst-footer .removeVideo {
         text-align: right;
         font-weight: 600;
         font-size: 13px;
         cursor: pointer;
      }
      .additionalResponse-wrap .question-wrap > .row{
         margin-right: -5px;
         margin-left: -5px;
      }
      .additionalResponse-wrap .question-wrap > .row > div{
         padding-right: 5px;
         padding-left: 5px;
      }
      .additionalResponse-wrap .question-box{
         height: 76px;
         max-width: 76px;
         margin-bottom: 10px;
      }
      .additionalResponse-wrap .question-box .icon img{
         max-width: 20px;
      }
      .additionalResponse-wrap .question-box .icon img {
         max-width: 42px;
      }
      .additionalResponse-wrap .question-box.qb-3 .icon img {
         max-width: 28px;
      }
      .submitResponse {
         position: fixed;
         bottom: 0;
         left: 0;
         right: 0;
         z-index: 99;
         padding: 20px;
         background: #f7f7f7;
         max-width: 1024px;
         margin: 0 auto;
      }
      .page-wrap.submitOne-page, .page-wrap.submitTwo-page{
         margin-bottom: 50px;
      }
      /*========================================
      Submit Two Page
      =========================================*/
      .post-text .PstText {
         border: solid 1px #dadada;
         border-radius: 4px;
         background: #fff;
         font-size: 20px;
         font-weight: 600;
         line-height: 26px;
         padding: 18px 17px;
      }
      /*========================================
      Thankyou page
      =========================================*/
      .thankyou-title{
         text-align: center;
      }
      .thankyou-title h1 {
         font-size: 50px;
         font-weight: 600;
         color: #fff;
         margin: 0;
         padding-top: 40px;
      }
      .thankyou-title p {
         font-size: 23px;
         font-weight: 600;
         color: #000000;
         margin-bottom: 1.5rem;
      }
      .sharebtn-wrap .share-btn{
         color:#fff;
         vertical-align: middle;
         text-align: left;
         background-color: #000000;
         display: flex;
      }
      .emilbox-wrap{
          
      text-align: center;
          
      margin-top: 2.5rem;
      }
      .emilbox-wrap h3{
          font-size: 1.3rem;
          color: #000;
      }
      .emilbox-wrap form{
          
      margin-top: 1.5em;
      }
      .emilbox-wrap form label{
          display: block;
          text-align:left;
          color: #000;
          margin-bottom: .1rem;
      }
      .emilbox-wrap .form-group {
          margin-bottom: .5rem;
      }
      .email-input {
          height: 44px;
      }
      .email-input:focus {
          border-color: #111;
          box-shadow: 0 0 0 0.2rem rgba(0,0,0,.25);
      }
      .btn.send-btn{
         color: #000;
         display: block;
         text-align: center;
         width: 100%;
         border: 2px solid #fff;
         font-weight: bold;
         font-size: 1.2rem;
         margin-bottom: 16px;
         background: transparent;
      }
      .btn.send-btn:hover{
         color: #fff;
         background: #000;
      }
      .post-text .PstText {
         border: solid 1px #dadada;
         border-radius: 4px;
         background: #fff;
         font-size: 20px;
         font-weight: 600;
         line-height: 26px;
         padding: 18px 17px;
      }
      .dflex {display: flex;align-items: center;justify-content: center;margin: 0 0 2em;}
      .dflex .col-3 {width:200px;max-width:100%;}
      .video_add {    background-color: #fff;
         padding: 10px;
         border-radius: 10px;
         border: 1px solid #ddd;margin:0 0 1em;}
      .m-0 {margin:0;}
      @media (max-width:767px) { 
         .dflex {display:block !important;} 
         .dflex .col-3 {margin:0 auto;}
   
       }
   
      </style>
      <div id="wrapper">
       <div class="page-wrap submitOne-page litePage-bgcolor">
            <header id="siteHeader" class="topHeader">
               <div class="container-fluid">
                  <div class="logo-header">
                     <a href="#"><img src="assets/images/logo-header.png" alt="Logo"/></a>
                  </div>
               </div>
            </header>
            <div id="main">
               <div class="container-fluid">
                  <div class="submitOne-section">
                     <div class="section-title">
                        <h1>[[question]]</h1>
                     </div>
                     <div class="postList-wrap">
                        <div class="post-items post-video">
                         <div class="PstVideo video_add">
                              <!--== ADDED VIDEO START ==-->
                              <video width="100%" controls>
                                 <source src="https://buzzable-media.s3.ap-south-1.amazonaws.com/a.mp4?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIOG6PW63K6IGGBFQ%2F20190924%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20190924T071809Z&X-Amz-Expires=900&X-Amz-Signature=641e615c40a4e323439a241783b56646d8c846bed8f2e2c2429c9abcd401cf01&X-Amz-SignedHeaders=host" type="video/mp4">
                                 
                                 Your browser does not support HTML5 video.
                              </video>
                              <!--== ADDED VIDEO END ==-->
                           </div>
                           <div class="video_add">
                              <!--== ADDED IMAGE START ==-->
                                 <img src="https://buzzable-media.s3.ap-south-1.amazonaws.com/ritik" alt="" />
                              <!--== ADDED IMAGE END ==-->
                           </div>
                           <div class="PstVideo video_add">
                              <!--== ADDED AUDIO START ==-->
                              <audio style="width:100%;" width="100%" controls>
                                 <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="audio/mp4">
                                 <source src="https://www.w3schools.com/html/mov_bbb.ogg" type="audio/ogg">
                                 Your browser does not support HTML5 audio.
                              </audio>
                              <!--== ADDED AUDIO END ==-->
                           </div>

                           
                           <div class="pst-footer">
                              <div class="removeVideo lite-color text-right">Remove</div>   
                           </div>
                        </div>
                     </div>
                     <div class="additionalResponse-wrap mb-5">
                     
                     <div class="section-title post-text">
                        <div class="PstText">
                        <p class="m-0">[[response]]</p>
                        </div>
                        
                        </div>
                        
                        
                        <div class="question-wrap">
                        <div class="section-title">
                           <p>Add additional response?</p>
                        </div>
                           <div class="row dflex">
                              <div class="col-3 col-md-3">
                                 <div class="question-box qb-1">
                                    <div class="icon">
                                       <img  src="assets/images/question-video.png" alt="Video Icon"/>
                                    </div>
                                 </div>
                              </div><!-- /.col-6 -->
                              <div class="col-3 col-md-3">
                                 <div class="question-box qb-2">
                                    <div class="icon">
                                       <img src="assets/images/question-image.png" alt="Image Icon"/>
                                    </div>
                                 </div>
                              </div><!-- /.col-6 -->
                              <div class="col-3 col-md-3">
                                 <div class="question-box qb-3">
                                    <div class="icon">
                                       <img src="assets/images/question-audio.png" alt="Audio Icon"/>
                                    </div>
                                 </div>
                              </div><!-- /.col-6 -->
                              <div class="col-3 col-md-3">
                                 <div class="question-box qb-4">
                                    <div class="icon">
                                       <img src="assets/images/question-text.png" alt="Text Icon"/>
                                    </div>
                                 </div>
                              </div><!-- /.col-6 -->
                           </div>
                        </div>
                     </div>
                     <div class="submitResponse">
                        <a href="/myView5">
							<button type="button" class="btn button-lite button-primary">Submit Response</button>
						</a>
                     </div>
                  </div><!-- /.question-section -->
               </div>
            </div><!-- /#main -->
            <footer id="siteFooter" class="setBottom">
               <div class="ftr-home container-fluid">
                  <h5>Your voice counts</h5>
                  <p class="lite-color">100% anonymous. 100% private.</p>
               </div>
            </footer>
         </div>
      </div>
    `}ready(){super.ready();//const Obj  = store.getState()
//this.question = Obj.question.question
// const storageResponse = JSON.parse(localStorage.getItem('responseData'))
// console.log("this.question ppppppppp ",storageResponse)
// this.response = storageResponse
const storageSessionData=JSON.parse(localStorage.getItem("responseData")),mediaVideo=JSON.parse(localStorage.getItem("mediaVideo"));console.log("mediaVideo========",mediaVideo);// this.video=document.URL.createObjectURL(mediaVideo)
this.video=changePath(mediaVideo);console.log("this.video",this.video);this.response=storageSessionData;const storageQuestion=JSON.parse(localStorage.getItem("questionData"));this.question=storageQuestion}/* _counterIncremented() {
       store.dispatch(increment());
     }
      _counterDecremented() {
       store.dispatch(decrement());
     }
      // This is called every time something is updated in the store.
     stateChanged(state) {
       this._clicks = state.counter.clicks;
       this._value = state.counter.value;
     } */}function changePath(mediaVideo){console.log("media value form function ",mediaVideo);// var data=URL.createObjectURL(mediaVideo);
// var binaryData = [];
// binaryData.push(mediaVideo);
// var data=window.URL.createObjectURL(new Blob(binaryData, {type: "application/zip"}))
// console.log('data===================',data);
// return mediaVideo.replace(/\\/g, "/")
// var datatwo=window.URL.revokeObjectURL(data)
// console.log("datatwo",datatwo)
// var reader=new FileReader();
// reader.readAsDataURL(mediaVideo);
// return reader.result;
}window.customElements.define("my-view4",MyView4)});