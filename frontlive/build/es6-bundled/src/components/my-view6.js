define(["./my-app.js"],function(_myApp){"use strict";_myApp.store.addReducers({shop:_myApp.$shopDefault});// These are the elements needed by this element.
class MyView6 extends _myApp.PageViewElement{static get properties(){return{// This is the data from the store.
_quantity:{type:Number},_error:{type:String}}}static get styles(){return[_myApp.SharedBootstrap,_myApp.ButtonSharedStyles,_myApp.SharedStyles]}render(){return _myApp.html$1`
     <div id="wrapper">
          <div class="page-wrap responseVideo-page litePage-bgcolor">
            <div class="response-video">
                <a href="/view2" ><img src="assets/images/response_video.png" alt="Response Audio"/></a>
            </div>
          </div>
      </div>
    `}/* _checkoutButtonClicked() {
       store.dispatch(checkout());
     }
      // This is called every time something is updated in the store.
     stateChanged(state) {
       this._quantity = cartQuantitySelector(state);
       this._error = state.shop.error;
     } */}window.customElements.define("my-view6",MyView6)});