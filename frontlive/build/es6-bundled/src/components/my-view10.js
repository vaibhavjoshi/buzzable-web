define(["./my-app.js"],function(_myApp){"use strict";_myApp.store.addReducers({shop:_myApp.$shopDefault});// These are the elements needed by this element.
class MyView10 extends(0,_myApp.connect)(_myApp.store)(_myApp.PageViewElement){static get properties(){return{// This is the data from the store.
_quantity:{type:Number},_error:{type:String}}}static get styles(){return[_myApp.SharedBootstrap,_myApp.ButtonSharedStyles,_myApp.SharedStyles]}render(){return _myApp.html$1`
      
    `}_checkoutButtonClicked(){_myApp.store.dispatch((0,_myApp.checkout)())}// This is called every time something is updated in the store.
stateChanged(state){this._quantity=(0,_myApp.cartQuantitySelector)(state);this._error=state.shop.error}}window.customElements.define("my-view10",MyView10)});