define(["./my-app.js"],function(_myApp){"use strict";function _templateObject_12d2c3c0e42b11e9b6da4fbb1c6f8049(){var data=babelHelpers.taggedTemplateLiteral(["\n     <div id=\"wrapper\">\n          <div class=\"page-wrap responseVideo-page litePage-bgcolor\">\n            <div class=\"response-video\">\n                <a href=\"/view2\" ><img src=\"assets/images/response_video.png\" alt=\"Response Audio\"/></a>\n            </div>\n          </div>\n      </div>\n    "]);_templateObject_12d2c3c0e42b11e9b6da4fbb1c6f8049=function _templateObject_12d2c3c0e42b11e9b6da4fbb1c6f8049(){return data};return data}_myApp.store.addReducers({shop:_myApp.$shopDefault});// These are the elements needed by this element.
var MyView6=/*#__PURE__*/function(_PageViewElement){babelHelpers.inherits(MyView6,_PageViewElement);function MyView6(){babelHelpers.classCallCheck(this,MyView6);return babelHelpers.possibleConstructorReturn(this,babelHelpers.getPrototypeOf(MyView6).apply(this,arguments))}babelHelpers.createClass(MyView6,[{key:"render",value:function render(){return(0,_myApp.html$1)(_templateObject_12d2c3c0e42b11e9b6da4fbb1c6f8049())}/* _checkoutButtonClicked() {
       store.dispatch(checkout());
     }
      // This is called every time something is updated in the store.
     stateChanged(state) {
       this._quantity = cartQuantitySelector(state);
       this._error = state.shop.error;
     } */}],[{key:"properties",get:function get(){return{// This is the data from the store.
_quantity:{type:Number},_error:{type:String}}}},{key:"styles",get:function get(){return[_myApp.SharedBootstrap,_myApp.ButtonSharedStyles,_myApp.SharedStyles]}}]);return MyView6}(_myApp.PageViewElement);window.customElements.define("my-view6",MyView6)});