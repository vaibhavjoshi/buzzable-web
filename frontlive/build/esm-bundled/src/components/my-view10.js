import{html$1 as html,PageViewElement,connect,store,checkout,$shopDefault as shop,cartQuantitySelector,SharedStyles,SharedBootstrap,ButtonSharedStyles}from"./my-app.js";store.addReducers({shop});// These are the elements needed by this element.
class MyView10 extends connect(store)(PageViewElement){static get properties(){return{// This is the data from the store.
_quantity:{type:Number},_error:{type:String}}}static get styles(){return[SharedBootstrap,ButtonSharedStyles,SharedStyles]}render(){return html`
      
    `}_checkoutButtonClicked(){store.dispatch(checkout())}// This is called every time something is updated in the store.
stateChanged(state){this._quantity=cartQuantitySelector(state);this._error=state.shop.error}}window.customElements.define("my-view10",MyView10);