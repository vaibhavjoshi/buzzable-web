import{html$1 as html,PageViewElement,SharedStyles}from"./my-app.js";class MyMatter extends PageViewElement{static get styles(){return[SharedStyles]}static get properties(){return{logindata:{type:Object},myString:{type:String},myArray:{type:Array},myBool:{type:Boolean}}}constructor(){super();this.logindata={username:""};this.myString="Hello World";this.myArray=["an","array","of","test","data"];this.myBool=!0}_clickHandler(e){console.log(e);console.log(this.logindata);return!1}render(){return html`
		<div class="container">
			<div class="row">
				<div class="span12">
					<div id="legend">
					  <legend class="">Add Matter</legend>
					</div>
					<div class="control-group">
					  <!-- Username -->
					  <label class="control-label"  for="username">Matter Heading</label>
					  <div class="controls">
						<input type="text" id="username" name="username" value='${this.logindata.username}' class="input-xlarge">
					  </div>
					</div>
					<div class="control-group">
					  <!-- Password-->
					  <label class="control-label" for="password">Matter Description</label>
					  <div class="controls">
						<textarea type="password" id="password" name="password" placeholder="" class="input-xlarge"></textarea>
					  </div>
					</div>
					<div class="control-group">
					  <!-- Button -->
					  <div class="controls">
						<button class="btn btn-success" @click="${this._clickHandler}">Login</button>
					  </div>
					</div>
				</div>
			</div>
		</div>
    `}}window.customElements.define("my-matter",MyMatter);